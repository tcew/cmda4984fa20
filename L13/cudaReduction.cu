
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

// to compile (on pascal gpu) : nvcc -O3 -arch=sm_61 -o cudaReduction cudaReduction.cu  -lm
// to run: ./cudaReduction N

// partial sum(s) of N entries from a 

// strawman reduction
// 1 thread
__global__ void reduction01(int N, float *a, float *suma){

  float sum = 0;
  for(int n=0;n<N;++n){
    sum += a[n];
  }
  
  suma[0] = sum;
}


double launchReduction01(int N, float *c_a, float *c_suma, float *h_suma){

  dim3 G(1); // 1 thread-block
  dim3 B(1); // 1 thread

  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaEventRecord(tic);
  
  reduction01 <<< G, B >>> (N, c_a, c_suma);
  
  cudaMemcpy(h_suma, c_suma, 1*sizeof(float), cudaMemcpyDeviceToHost);

  cudaEventRecord(toc);

  cudaDeviceSynchronize();
  
  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);

  return (double) elapsed/1000.;
  
}

// no collaboration, one output per thread
// M threads, M << N
__global__ void reduction02(int N, float *a, float *suma){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;
  int G = gridDim.x;

  int M = G*B; // total number of threads
  
  float sum = 0;

  int n = t + b*B;
  
  while(n<N){
    sum += a[n];
    n += M;
  }

  // need to reset the counter
  n = t + b*B;
  suma[n] = sum;
}


double launchReduction02(int N, float *c_a, float *c_suma, float *h_suma){

  int T = 256;
  int Nblocks = 28*4;
  int M = T*Nblocks; // 
  
  dim3 G(Nblocks);
  dim3 B(T);      

  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaEventRecord(tic);
  
  reduction02 <<< G, B >>> (N, c_a, c_suma);

  cudaMemcpy(h_suma, c_suma, M*sizeof(float), cudaMemcpyDeviceToHost);

  for(int n=1;n<M;++n){
    h_suma[0] += h_suma[n];
  }
  
  cudaEventRecord(toc);

  cudaDeviceSynchronize();
  
  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);

  return (double) elapsed/1000.;
  
}

#define FULL_MASK 0xffffffff

#define NWARPS 32

__device__ float blockShflReduction(float sum){
  
  sum += __shfl_down_sync(FULL_MASK, sum, 16);
  sum += __shfl_down_sync(FULL_MASK, sum,  8);
  sum += __shfl_down_sync(FULL_MASK, sum,  4);
  sum += __shfl_down_sync(FULL_MASK, sum,  2);
  sum += __shfl_down_sync(FULL_MASK, sum,  1);

  return sum;
}

// thread-block collaboration, one output per thread-block
__global__ void reduction03(int N, float *a, float *suma){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;
  int G = gridDim.x;

  int M = G*B; // total number of threads
  
  float sum = 0;

  int n = t + b*B;
  
  while(n<N){
    sum += a[n];
    n += M;
  }

  // assume 32 threads per warp
  sum = blockShflReduction(sum);
  
  int l = t%32;
  int w = t>>5; // divide by 32

  // now need to share the root values from each warp into the 0 warp
  __shared__ float s_sum[NWARPS];

  if(l==0){
    s_sum[w] = sum;
  }

  // sync here because we intend to read - and the write involved all warps
  __syncthreads(); 

  // nominate one warp to complete the reduction
  if(w==0){ // make warp 0 finalize the block reduction
    sum = s_sum[l];

    sum = blockShflReduction(sum);

    if(t==0){
      suma[b] = sum;
    }
  }
}


double launchReduction03(int N, float *c_a, float *c_suma, float *h_suma){

  int T = NWARPS*32;
  //  int Nblocks = 28*4;
  int Nblocks = 28*6;
  
  dim3 G(Nblocks);
  dim3 B(T);      

  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaEventRecord(tic);
  
  reduction03 <<< G, B >>> (N, c_a, c_suma);

  cudaMemcpy(h_suma, c_suma, Nblocks*sizeof(float), cudaMemcpyDeviceToHost);

  for(int n=1;n<Nblocks;++n){
    h_suma[0] += h_suma[n];
  }
  
  cudaEventRecord(toc);

  cudaDeviceSynchronize();
  
  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);

  return (double) elapsed/1000.;
  
}

// shared reduction
__global__ void reduction04(int N, float *a, float *suma){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;
  int G = gridDim.x;

  int M = G*B; // total number of threads
  
  float sum = 0;

  int n = t + b*B;
  
  while(n<N){
    sum += a[n];
    n += M;
  }

  int l = t%32;
  int w = t>>5; // divide by 32
  
  // do a sequence of warp shfls to reduce the "sum" registers further

  volatile __shared__ float s_psum[NWARPS][32];
  
  s_psum[w][l] = sum;
  
  // assume 32 threads per warp are synced
  if(l<16) s_psum[w][l] += s_psum[w][l+16]; // DANGEROUS ASSUMPTION OF WARP SYNCHRONY
  __syncwarp();

  if(l< 8) s_psum[w][l] += s_psum[w][l+ 8];
  __syncwarp();
  
  if(l< 4) s_psum[w][l] += s_psum[w][l+ 4];
  __syncwarp();
  
  if(l< 2) s_psum[w][l] += s_psum[w][l+ 2];
  __syncwarp();
  
  if(l< 1) s_psum[w][l] += s_psum[w][l+ 1];

  // sync here because we intend to read - and the write involved all warps
  __syncthreads(); 

  volatile __shared__ float s_psum2[32];
  
  // nominate one warp to complete the reduction
  if(w==0){ // make warp 0 finalize the block reduction
    s_psum2[l] = s_psum[l][0];

    if(l<16) s_psum2[l] += s_psum2[l+16]; // DANGEROUS ASSUMPTION OF WARP SYNCHRONY
    __syncwarp();
    
    if(l< 8) s_psum2[l] += s_psum2[l+ 8];
    __syncwarp();
    
    if(l< 4) s_psum2[l] += s_psum2[l+ 4];
    __syncwarp();
    
    if(l< 2) s_psum2[l] += s_psum2[l+ 2];
    __syncwarp();
    
    if(l< 1) s_psum2[l] += s_psum2[l+ 1];
    __syncwarp();
    
    if(t==0){
      suma[b] = s_psum2[0];
    }
  }
}

double launchReduction04(int N, float *c_a, float *c_suma, float *h_suma){

  int T = NWARPS*32;
  int Nblocks = 28*6;
  
  dim3 G(Nblocks);
  dim3 B(T);      

  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaEventRecord(tic);
  
  reduction04 <<< G, B >>> (N, c_a, c_suma);

  cudaMemcpy(h_suma, c_suma, Nblocks*sizeof(float), cudaMemcpyDeviceToHost);
  
  for(int n=1;n<Nblocks;++n){
    h_suma[0] += h_suma[n];
  }
  
  cudaEventRecord(toc);

  cudaDeviceSynchronize();
  
  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);

  return (double) elapsed/1000.;
  
}



int main(int argc, char **argv){

  int N = atoi(argv[1]);

  float *h_a, *h_suma, *c_a, *c_suma;

  h_a    = (float*) calloc(N, sizeof(float));
  h_suma = (float*) calloc(N, sizeof(float));
  for(int n=0;n<N;++n)
    h_a[n] = 1;
  
  cudaMalloc(&c_a,    N*sizeof(float));
  cudaMalloc(&c_suma, N*sizeof(float));

  cudaMemcpy(c_a, h_a, N*sizeof(float), cudaMemcpyHostToDevice);
  
  int Nkernels = 10;
  double *elapsed = (double*) calloc(Nkernels, sizeof(double));

  for(int test=0;test<1;++test){
    int knl = 0;
    elapsed[knl] = launchReduction01(N, c_a, c_suma, h_suma);
    printf("kernel %d took %g s (result=%g) \n", knl+1, elapsed[knl], h_suma[0]);
    knl++;
    
    elapsed[knl] = launchReduction02(N, c_a, c_suma, h_suma);
    printf("kernel %d took %g s (result=%g) \n", knl+1, elapsed[knl], h_suma[0]);
    knl++;
    
    elapsed[knl] = launchReduction03(N, c_a, c_suma, h_suma);
    printf("kernel %d took %g s (result=%g) \n", knl+1, elapsed[knl], h_suma[0]);
    knl++;
    
    elapsed[knl] = launchReduction04(N, c_a, c_suma, h_suma);
    printf("kernel %d took %g s (result=%g) \n", knl+1, elapsed[knl], h_suma[0]);
    knl++;
  }


  return 0;
}
