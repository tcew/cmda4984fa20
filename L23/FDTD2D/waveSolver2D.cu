#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>
#include <cuda_fp16.h>
#include <mma.h>

using namespace nvcuda;

// to compile in CUDA: nvcc -arch=sm_70 -o waveSolver2D waveSolver2D.cu
// to run reference test: ./waveSolver2D 1000 -1.0 1.0 2.0 3

#define SIMD 32
#define FULL_MASK 0xFFFFFFFF
#define NY 2

// hard coded for NVIDIA GTX 1080 TI
#define NBLOCKS (28*6)

int index(int N, int i, int j){
  return i + j*(N+2);
}

// use second order finite difference in time and space to update solution at entry n
void update(int N, float lambda, float *h_u, float *h_v){

  for(int j=1;j<=N;++j){
    for(int i=1;i<=N;++i){
      int ijC = index(N, i, j); // i + j*(N+2);
      int ijN = index(N, i, j+1); // i + (j+1)*(N+2);
      int ijS = i + (j-1)*(N+2);
      int ijE = i+1 + j*(N+2);
      int ijW = i-1 + j*(N+2);

      float uCold = h_v[ijC];
      float uC    = h_u[ijC];
      
      h_v[ijC] = 2.f*uC - uCold
	+ lambda*(h_u[ijE]-2.f*uC+h_u[ijW])
	+ lambda*(h_u[ijN]-2.f*uC+h_u[ijS]);
    }
  }
}

// V0: linear thread indexing
__global__ void updateKernelV0(int N, float lambda, float *c_u, float *c_v){

  // ij = i + j*(N+2)
  int ij = threadIdx.x + blockDim.x*blockIdx.x;

  int ijC = ij;
  int ijN = ij + (N+2);
  int ijS = ij - (N+2);
  int ijE = ij + 1; 
  int ijW = ij - 1;

  // potentially expensive integer operations
  int i = ij % (N+2); // modulo to get i from ij
  int j = ij / (N+2); // integer division to get j from ij

  if(i>0 && i<N+1 && j>0 && j<N+1){

    float uCold = c_v[ijC];
    float uC    = c_u[ijC];
    
    c_v[ijC] = 2.f*uC - uCold
      + lambda*(c_u[ijE]-2.f*uC+c_u[ijW])
      + lambda*(c_u[ijN]-2.f*uC+c_u[ijS]);
  }
}

// V0 launcher
void runUpdateKernelV0(int N, float lambda, float *c_u, float *c_v){

  int Nall = (N+2)*(N+2);
  int T = SIMD*SIMD;
  dim3 G( (Nall+T-1)/T );
  dim3 B(T);

  updateKernelV0 <<<G,B>>> (N, lambda, c_u, c_v);
}


// V1: no manual caching or optimization
__global__ void updateKernelV1(int N, float lambda, float *c_u, float *c_v){

  int i = threadIdx.x + blockDim.x*blockIdx.x;
  int j = threadIdx.y + blockDim.y*blockIdx.y;
  int ij = i +     j*(N+2);
  int ijC = ij;
  int ijN = ij +(N+2);
  int ijS = ij -(N+2);
  int ijE = ij + 1;
  int ijW = ij - 1;

  if(i>0 && i<N+1 && j>0 && j<N+1){

    float uCold = c_v[ijC];
    float uC    = c_u[ijC];
    
    c_v[ijC] = 2.f*uC - uCold
      + lambda*(c_u[ijE]-2.f*uC+c_u[ijW])
      + lambda*(c_u[ijN]-2.f*uC+c_u[ijS]);
  }
}

// V1 launcher
void runUpdateKernelV1(int N, float lambda, float *c_u, float *c_v){

  dim3 G( (N+2 + SIMD-1)/SIMD, (N+2 + SIMD-1)/SIMD);
  dim3 B(SIMD,SIMD);

  updateKernelV1 <<<G,B>>> (N, lambda, c_u, c_v);
}


// V2: use shared cache
__global__ void updateKernelV2(int N, float lambda, float *c_u, float *c_v){

  int tx = threadIdx.x;
  int ty = threadIdx.y;
  
  int i = tx + (SIMD-2)*blockIdx.x;
  int j = ty + (SIMD-2)*blockIdx.y;

  int ijC = i +     j*(N+2);

  __shared__ float s_u[SIMD][SIMD];

  if(i<N+2 && j<N+2){
    s_u[ty][tx] = c_u[ijC];
  }
  
  __syncthreads();
  
  if(i>0 && i<N+1 && j>0 && j<N+1){
    if(tx>0 && tx<SIMD-1 && ty>0 && ty<SIMD-1){
      
      float uCold = c_v[ijC];
      float uC    = s_u[ty][tx];
      
      c_v[ijC] = 2.f*uC - uCold
	+ lambda*(s_u[ty][tx+1] - 2.f*uC + s_u[ty][tx-1])
	+ lambda*(s_u[ty+1][tx] - 2.f*uC + s_u[ty-1][tx]);
    }
  }
}

// V2 launcher
void runUpdateKernelV2(int N, float lambda, float *c_u, float *c_v){

  dim3 G( (N+2 + SIMD-3)/(SIMD-2), (N+2 + SIMD-3)/(SIMD-2));
  dim3 B(SIMD,SIMD);

  updateKernelV2 <<<G,B>>> (N, lambda, c_u, c_v);
}


// V3: use warp synchronous shfl ops. Requires shared transpose
__global__ void updateKernelV3(int N, float lambda, float *c_u, float *c_v){

  // pad to avoid bank conflicts
  __shared__ float s_u[SIMD][SIMD+1];
  
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int i = tx + (SIMD-2)*blockIdx.x;
  int j = ty + (SIMD-2)*blockIdx.y;
  int l = threadIdx.x; // lane
  int w = threadIdx.y; // warp
  
  int ijC = i + j*(N+2);

  float uC = (i<N+2 && j<N+2) ? c_u[ijC] : 0;
  float uE = __shfl_down_sync(FULL_MASK, uC, 1);
  float uW = __shfl_up_sync(FULL_MASK, uC, 1);

  float resEW = uE+uW;
  
  s_u[tx][ty] = uC;

  __syncthreads();

  // transpose
  float uCT  = s_u[ty][tx];

  // compute N+S
  float uN = __shfl_down_sync(FULL_MASK, uCT, 1);
  float uS = __shfl_up_sync(FULL_MASK, uCT, 1);

  // share to transpose again
  s_u[ty][tx] = uN+uS;
  
  __syncthreads();

  // make sure thread is interior to thread-block and interior to domain
  if(i>0 && i<N+1 && j>0 && j<N+1 && l>0 && l<31 && w>0 && w<31){
    float uCold = c_v[ijC];
    c_v[ijC] = (2-4.*lambda)*uC - uCold + lambda*resEW + lambda*s_u[tx][ty];
  }
}

// V3 launcher
void runUpdateKernelV3(int N, float lambda, float *c_u, float *c_v){

  dim3 G( (N+2 + SIMD-3)/(SIMD-2), (N+2 + SIMD-3)/(SIMD-2));
  dim3 B(SIMD,SIMD);
  
  updateKernelV3 <<<G,B>>> (N, lambda, c_u, c_v);
}


// V4: bonus uses a single warp and marches through NY layers, shfling at every step
__global__ void updateKernelV4(int N, float lambda, float *c_u, float *c_v){

  int j = blockIdx.y*NY;
  int i = threadIdx.x + (SIMD-2)*blockIdx.x;
  int l = threadIdx.x;
  
  int ijN = i + (j+1)*(N+2);
  int ijS = i + (j-1)*(N+2);
  int ijC = i +     j*(N+2);

  float uS = (j>0 && j-1<(N+2) && i<(N+2)) ? c_u[ijS] : 0;
  float uC = (j>0 && j<(N+2)   && i<(N+2)) ? c_u[ijC] : 0;
  
  for(int js=0;js<NY;++js){

    float uN = (i<N+2 && j+1<N+2) ? c_u[ijN]: 0;
    
    float uW = __shfl_up_sync  (FULL_MASK, uC, 1);
    float uE = __shfl_down_sync(FULL_MASK, uC, 1);
    
    if(i>0 && i<N+1 && j>0 && j<N+1 && l>0 && l<31){
      float uCold = c_v[ijC];
      c_v[ijC] = 2.f*uC - uCold + lambda*(uE-2.f*uC+uW) + lambda*(uN-2.f*uC+uS);
    }

    ijC += (N+2);
    ijN += (N+2);
    
    uS = uC;
    uC = uN;
    
    ++j;
    
  }
}

// V4 launcher
void runUpdateKernelV4(int N, float lambda, float *c_u, float *c_v){
          
  dim3 G( (N+2 + SIMD-3)/(SIMD-2), (N+2 + NY-1 )/NY);
  dim3 B(SIMD,1);
  
  updateKernelV4 <<<G,B>>> (N, lambda, c_u, c_v);
}


// V5: use wmma to do warp matrix multiply add
#define DIM 16
__global__ void updateKernelV5(int N, float lambda, float *c_u, float *c_v){

  int i0 = (DIM-2)*blockIdx.x;
  int j0 = (DIM-2)*blockIdx.y;

  __shared__ half s_u[DIM][DIM];
  __shared__ half s_L[DIM][DIM];
  __shared__ float s_ans[DIM][DIM];

  int n = threadIdx.x;
  while(n<DIM*DIM){
    int ioff = n%DIM;
    int joff = n/DIM;
    int i = i0 + ioff;
    int j = j0 + joff;
    int ij = i + (N+2)*j;
    float uij = (i<(N+2) && j<(N+2)) ? c_u[ij]: 0;
    s_u[joff][ioff] = __float2half(uij);

    float Lij = 0;
    if(ioff-1==joff) Lij = lambda;
    if(ioff+0==joff) Lij = -2.f*lambda;
    if(ioff+1==joff) Lij = lambda;

    s_L[joff][ioff] = __float2half(Lij);
    
    n += SIMD;
  }

#if 0
  __syncwarp();

  if(i0==0 && j0==0 && threadIdx.x==0){
    printf("L=[\n");
    for(int n=0;n<DIM;++n){
      for(int m=0;m<DIM;++m){
	printf("%f ", __half2float(s_L[n][m]));
      }
      printf("\n");
    }
    printf("];");
  }
#endif
  __syncwarp();
  
  wmma::fragment<wmma::accumulator, DIM, DIM, DIM, float> ans_frag;

  {
    // Declare the fragments (for L*u)
    wmma::fragment<wmma::matrix_a, DIM, DIM, DIM, half, wmma::row_major> L_frag;
    wmma::fragment<wmma::matrix_b, DIM, DIM, DIM, half, wmma::row_major> u_frag;

    // load fragments
    wmma::load_matrix_sync(L_frag, s_L[0], DIM);
    wmma::load_matrix_sync(u_frag, s_u[0], DIM);
    wmma::fill_fragment(ans_frag, 0.0f);
    
    // ans = L*u
    wmma::mma_sync(ans_frag, L_frag, u_frag, ans_frag);
  }

  {
    // Declare the fragments (for u*L)
    wmma::fragment<wmma::matrix_a, DIM, DIM, DIM, half, wmma::row_major> u_frag;
    wmma::fragment<wmma::matrix_b, DIM, DIM, DIM, half, wmma::row_major> L_frag;
    
    wmma::load_matrix_sync(L_frag, s_L[0], DIM);
    wmma::load_matrix_sync(u_frag, s_u[0], DIM);
    
    // ans +=L*u
    wmma::mma_sync(ans_frag, u_frag, L_frag, ans_frag);
  }


  wmma::store_matrix_sync(s_ans[0], ans_frag, DIM, wmma::mem_row_major);
  
  // write result
  n = threadIdx.x;
  while(n<DIM*DIM){
    int ioff = n%DIM;
    int joff = n/DIM;
    int i = i0 + ioff;
    int j = j0 + joff;
    int ij = i + (N+2)*j;
    if(ioff>0 && joff>0 && ioff<DIM-1 && joff<DIM-1){
      if(i>0 && j>0 && i<(N+1) && j<(N+1)){
	c_v[ij] = 2.f*c_u[ij] - c_v[ij] + s_ans[joff][ioff];
      }
    }

    n += SIMD;
  }
}

// V5 launcher
void runUpdateKernelV5(int N, float lambda, float *c_u, float *c_v){
          
  dim3 G( (N+2 + DIM-2 - 1)/(DIM-2), (N+2 + DIM-2-1 )/(DIM-2));
  dim3 B(SIMD,1);
  
  updateKernelV5 <<<G,B>>> (N, lambda, c_u, c_v);
}




void runUpdateKernel(int N, float lambda, float *c_u, float *c_v, int knl){

  switch(knl){
  case 0: runUpdateKernelV0(N, lambda, c_u, c_v); break;
  case 1: runUpdateKernelV1(N, lambda, c_u, c_v); break;
  case 2: runUpdateKernelV2(N, lambda, c_u, c_v); break;
  case 3: runUpdateKernelV3(N, lambda, c_u, c_v); break;
  case 4: runUpdateKernelV4(N, lambda, c_u, c_v); break;
  case 5: runUpdateKernelV5(N, lambda, c_u, c_v); break;
  default: printf("knl %d not available exiting\n", knl); exit(-1);
  }
  
}


  // initialize finite difference nodes
void setup(int N,
	   float xmin, float xmax,
	   float ymin, float ymax,
	   float finalTime,
	   float **h_x, float **h_y,
	   float **h_u, float **h_v, float **h_sumu,
	   float **c_u, float **c_v, float **c_sumu,
	   float *dt, float *h){

  int Nall = (N+2)*(N+2);
  
  // initialize storage (adding boundary nodes at either end)
  float *x = (float*) calloc(Nall, sizeof(float));
  float *y = (float*) calloc(Nall, sizeof(float));
  float *u = (float*) calloc(Nall, sizeof(float));
  float *v = (float*) calloc(Nall, sizeof(float));
  float *sumu = (float*) calloc(NBLOCKS, sizeof(float));

  cudaMalloc(c_u, Nall*sizeof(float));
  cudaMalloc(c_v, Nall*sizeof(float));
  cudaMalloc(c_sumu, NBLOCKS*sizeof(float));
  
  *h = (xmax-xmin)/(N+1);
  printf("h=%17.15le\n", *h);

  // choose stable time step
  *dt = 0.25*(*h);
  
  // adjust dt to take an even integer number of steps
  int Nsteps = ceil(finalTime/(*dt));
  Nsteps = 2*ceil(Nsteps/2.);
  *dt = finalTime/Nsteps;
  
  // initialize coordinates, boundary conditions, and initial conditions
  for(int j=0;j<N+2;++j){
    for(int i=0;i<N+2;++i){
      // linear index of node (i,j)
      int ij = i + j*(N+2);
      // x-coordinate of node 
      x[ij] = xmin + (*h)*i;
      // y-coordinate of node 
      y[ij] = ymin + (*h)*j;
      // solution at time 0 at node ij
      u[ij] = 0;
      // compute distance of node from center of pulse
      float xo = .1;
      float yo = .05;
      float rij = sqrt(pow(x[ij]-xo,2)+pow(y[ij]-yo,2));
      float invrij = (rij) ? 1./rij:0;
      
      // solution at time -dt at node n
      float alpha = 140;
      u[ij] = 0;
      v[ij] = 0.2*(exp(-alpha*pow(rij+(*dt),2)) -
		     exp(-alpha*pow(rij-(*dt),2)))*invrij; 
    }
  }

  // pass pointers to calling function
  *h_x = x;
  *h_y = y;
  *h_u = u;
  *h_v = v;
  *h_sumu = sumu;

}

float checksum(int N, float *h_u){

  float sum = 0;
  for(int j=0;j<N+2;++j){
    for(int i=0;i<N+2;++i){
      int ij = i + j*(N+2);
      sum += h_u[ij]*h_u[ij];
    }
  }
  return sqrt(sum);
}

#define NWARPS 32

__device__ float blockShflReduction(float sum){
  
  sum += __shfl_down_sync(FULL_MASK, sum, 16);
  sum += __shfl_down_sync(FULL_MASK, sum,  8);
  sum += __shfl_down_sync(FULL_MASK, sum,  4);
  sum += __shfl_down_sync(FULL_MASK, sum,  2);
  sum += __shfl_down_sync(FULL_MASK, sum,  1);

  return sum;
}

// thread-block collaboration, one output per thread-block
__global__ void checksumKernel(int N, float *c_a, float *c_suma){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;
  int G = gridDim.x;

  int M = G*B; // total number of threads
  
  float sum = 0;

  int n = t + b*B;
  
  while(n<N){
    float an = c_a[n];
    sum += an*an;
    n += M;
  }
  
  // assume 32 threads per warp
  sum = blockShflReduction(sum);
  
  int l = t%32;
  int w = t>>5; // divide by 32

  // now need to share the root values from each warp into the 0 warp
  __shared__ float s_sum[NWARPS];

  if(l==0){
    s_sum[w] = sum;
  }

  // sync here because we intend to read - and the write involved all warps
  __syncthreads(); 

  // nominate one warp to complete the reduction
  if(w==0){ // make warp 0 finalize the block reduction
    sum = s_sum[l];

    sum = blockShflReduction(sum);

    if(t==0){
      c_suma[b] = sum;
    }
  }
}

float  runChecksumKernel(int N, float *c_a, float *c_suma, float *h_suma){

  int T = NWARPS*32;
  
  dim3 G(NBLOCKS);
  dim3 B(T);      

  checksumKernel <<< G, B >>> ((N+2)*(N+2), c_a, c_suma);

  cudaMemcpy(h_suma, c_suma, NBLOCKS*sizeof(float), cudaMemcpyDeviceToHost);

  for(int n=1;n<NBLOCKS;++n){
    h_suma[0] += h_suma[n];
  }

  return sqrt(h_suma[0]);
    
}

// time step
void run(int N, int knl, float finalTime, float dt, float h, float *h_u, float *h_v, float *h_sumu,
	 float *c_u, float *c_v, float *c_sumu){

  int Nsteps = finalTime/dt;

  printf("Nsteps=%d\n", Nsteps);
  printf("dt=%17.15le\n", dt);
  
  float lambda = pow(dt/h,2);

  size_t Nbytes = (N+2)*(N+2)*sizeof(float);
  
  cudaMemcpy(c_u, h_u, Nbytes, cudaMemcpyHostToDevice);
  cudaMemcpy(c_v, h_v, Nbytes, cudaMemcpyHostToDevice);

  // update recurrence formula twice
  for(int m=0;m<Nsteps;m+=2){

    // first update from u to v 
    //    update(N, lambda, h_u, h_v);
    runUpdateKernel (N, lambda, c_u, c_v, knl);

    // second update from v to u
    //    update(N, lambda, h_v, h_u);
    runUpdateKernel (N, lambda, c_v, c_u, knl);

    if((m%50)==0){ // checkpoint
      //      cudaMemcpy(h_u, c_u, Nbytes, cudaMemcpyDeviceToHost);
	  
      //      float l2norm = h*h*checksum(N, h_u);
      float l2norm = h*h*runChecksumKernel(N, c_u, c_sumu, h_sumu);
      //      printf("step: %d, solution norm: %17.15le\n", m+2, l2norm);
    }
  }

  cudaMemcpy(h_u, c_u, Nbytes, cudaMemcpyDeviceToHost);
  cudaMemcpy(h_v, c_v, Nbytes, cudaMemcpyDeviceToHost);
  
}


void save(const char *fname, int N, float *h_x, float *h_y, float *h_u){

  FILE *fp = fopen(fname, "w");

  for(int j=0;j<N+2;++j){
    for(int i=0;i<N+2;++i){
      int ij = i + j*(N+2);
      fprintf(fp, "%f %f %f\n", h_x[ij], h_y[ij], h_u[ij]);
    }
  }
  
  fclose(fp);
}


void test(int N, float finalTime, float dt, float h, float *h_u, float *h_v, float *c_u, float *c_v){

  float lambda = pow(dt/h,2);
  
  int Nall = (N+2)*(N+2);
  
  // seed with random data
  for(int n=0;n<Nall;++n){
    h_u[n] = drand48();
    h_v[n] = drand48();
  }
  cudaMemcpy(c_u, h_u, Nall*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_v, h_v, Nall*sizeof(float), cudaMemcpyHostToDevice);

  dim3 G( (N+2 + SIMD-3)/(SIMD-2), (N+2 + NY-1)/NY);
  dim3 B(SIMD,1);
  
  updateKernelV3 <<<G,B>>> (N, lambda, c_u, c_v);

  update(N, lambda, h_u, h_v);

  cudaMemcpy(h_u, c_v, Nall*sizeof(float), cudaMemcpyDeviceToHost);
  for(int i=0;i<N+2;++i) {
    for(int j=0;j<N+2;++j){
      int ij = i*(N+2)+j;
      if(fabs(h_u[ij]-h_v[ij])>1.e-11){
	printf("X");
      }
    }
  }
}

// Purpose: solve the linear second order wave equation using a second order finite difference
// in one spatial dimension using a domain [xmin,xmax] with Dirichlet boundary conditions at
// the boundary
int main(int argc, char **argv){

  // read arguments from command line
  if(argc<6) { printf("usage: ./waveSolver2D N xmin xmax finalTime kernel\n"); exit(-1); }

  int    N    = atoi(argv[1]); // number of points (N+2) total
  float xmin = atof(argv[2]); // x coordinate of left boundary
  float xmax = atof(argv[3]); // x coordinate of right boundary
  float finalTime = atof(argv[4]);  // final time
  int    knl  = atoi(argv[5]); // kernel number

  float ymin = xmin; // duplicate x range to y range
  float ymax = xmax;

  // set up finite difference nodes and initial solution
  float *h_x, *h_y, *h_u, *h_v, *h_sumu;
  float *c_u, *c_v, *c_sumu;
  float h, dt;
  setup(N, xmin, xmax, ymin, ymax, finalTime, &h_x, &h_y, &h_u, &h_v, &h_sumu, &c_u, &c_v, &c_sumu, &dt, &h);

  cudaEvent_t tic, toc;
  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaDeviceSynchronize();
  cudaEventRecord(tic);
  
  // run solver
  run(N, knl, finalTime, dt, h, h_u, h_v, h_sumu, c_u, c_v, c_sumu);

  cudaEventRecord(toc);
  cudaDeviceSynchronize();

  float elapsed = 0;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.;
  
  // print solution at final time
  save("solution.dat", N, h_x, h_y, h_u);

  // compute checksum
  //float sum = h*h*checksum(N, h_u);
  float sum = h*h*runChecksumKernel(N, c_u, c_sumu, h_sumu);
  printf("final checksum=%17.15le\n", sum);
  printf("elapsed time=%17.15le\n", elapsed);
  
  // clean up
  free(h_x); free(h_y); free(h_u); free(h_v);
  
  return 0;
}

  
