// https://stackoverflow.com/questions/52831987/how-to-use-wmma-functions

#include <cuda.h>
#include <cuda_fp16.h>
#include <mma.h>
#include <iostream>

using namespace nvcuda;

#define DIM 16

// probe storage convention for fragments
__global__ void probeKernel(){

  int t = threadIdx.x;
  
  // use same A for all thread-blocks, different B and C
  __shared__ half s_A[DIM][DIM], s_B[DIM][DIM];
  __shared__ float s_C[DIM][DIM];
  
  __shared__ int s_Athread[DIM][DIM];
  __shared__ int s_Bthread[DIM][DIM];
  __shared__ int s_Cthread[DIM][DIM];

  __shared__ int s_Aid[DIM][DIM];
  __shared__ int s_Bid[DIM][DIM];
  __shared__ int s_Cid[DIM][DIM];
  
  
  // Declare the fragments
  wmma::fragment<wmma::matrix_a, DIM, DIM, DIM, half, wmma::row_major> A_frag;
  wmma::fragment<wmma::matrix_b, DIM, DIM, DIM, half, wmma::col_major> B_frag;
  wmma::fragment<wmma::accumulator, DIM, DIM, DIM, float> C_frag;

  for(int r=0;r<DIM;++r){
    for(int c=0;c<DIM;++c){

      __syncwarp();
      
      if(t==0){ // set up one entry
	s_A[r][c] = 1; s_B[c][r] = 1; s_C[r][c] = 1;
      }

      __syncwarp();
  
      // Load the inputs
      wmma::load_matrix_sync(A_frag, s_A[0], DIM);
      wmma::load_matrix_sync(B_frag, s_B[0], DIM);

      // load the accumulator
      wmma::load_matrix_sync(C_frag, s_C[0], DIM, wmma::mem_row_major);

      
      if(t==0){ // reset the matrix
	s_A[r][c] = 0;	s_B[c][r] = 0;	s_C[r][c] = 0;
      }
  
      // each thread in the warp checks for non-zeros in its fragment
      for(int i=0; i < A_frag.num_elements; i++) {
	if(__half2float(A_frag.x[i])!=0){
	  s_Athread[r][c] = t;
	  s_Aid[r][c] = i;
	}
      }
      for(int i=0; i < B_frag.num_elements; i++) {
	if(__half2float(B_frag.x[i])!=0){
	  s_Bthread[r][c] = t;
	  s_Bid[r][c] = i;
	}
      }


      for(int i=0; i < C_frag.num_elements; i++) {
	if(C_frag.x[i]!=0){
	  s_Cthread[r][c] = t;
	  s_Cid[r][c] = i;
	}
      }
    }
  }
    
  if(t==0){
    printf("A=[\n");
    for(int r=0;r<DIM;++r){
      for(int c=0;c<DIM;++c){
	printf("%02d (%02d) ", s_Athread[r][c], s_Aid[r][c]);
      }
      printf("\n");
    }
    printf("B=[\n");
    for(int r=0;r<DIM;++r){
      for(int c=0;c<DIM;++c){
	printf("%02d (%02d) ", s_Bthread[r][c], s_Bid[r][c]);
      }
      printf("\n");
    }
    printf("C=[\n");
    for(int r=0;r<DIM;++r){
      for(int c=0;c<DIM;++c){
	printf("%02d (%02d) ", s_Cthread[r][c], s_Cid[r][c]);
      }
      printf("\n");
    }
  }

}


void probe(){

  probeKernel<<<1,dim3(32,1,1)>>>();

  cudaDeviceSynchronize();
}



int main(int argc, char **argv){

  if(argc!=1){ printf("usage: ./cudaTensorProbing \n"); exit(-1);}
  
  probe();

}
