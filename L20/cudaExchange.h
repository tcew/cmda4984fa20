
typedef struct {

  int N;

  // finite diff data
  float *c_data;

  int Nout; // amount of data outgoing from halo
  int Nin;  // amount of data incoming into halo
  
  // list of nodes to extract for outgoing halo
  int *c_indexOut;
  int *c_indexIn;

  // a host halo  array
  float *h_dataOut;
  float *h_dataIn;

  // DEVICE halo array
  float *c_dataOut;
  float *c_dataIn;

  // MPI message info
  int NmessagesOut;
  int *h_rankOut;
  int *h_NOut;

  int NmessagesIn;
  int *h_rankIn;
  int *h_NIn;

  
}cudaExchange_t;

cudaExchange_t *cudaExchangeInit(int N, int rank, int size, float *h_data);

void cudaExchangeExtract(cudaExchange_t *exch);

void cudaExchangeInject(cudaExchange_t *exch);

void cudaExchangeToHost(cudaExchange_t *exch, float *h_data);
