#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
//#include <cuda_runtime.h>

// to compile (must use recent CUDA version):
// nvcc -arch=sm_60 -o cudaGraph cudaGraph.cu 

__global__ void nothingKernel(int N){

}

int main(int argc, char **argv){
  
  // create stream for recording
  cudaStream_t stream;
  cudaStreamCreate(&stream);
  
  // cuda stream capture sequence for nothingKernel
  cudaGraph_t nothingGraph;

  cudaStreamBeginCapture(stream, cudaStreamCaptureModeGlobal);

  int N = 100;
  int Ntests = 10;
  for(int test=0;test<Ntests;++test){
    nothingKernel <<< 1, 1, 0, stream >>> (N);
  }
  
  cudaStreamEndCapture(stream, &nothingGraph);
  
  // time graph sequence for nothing
  cudaGraphExec_t nothingExecutableGraph;
  cudaGraphInstantiate(&nothingExecutableGraph, nothingGraph, NULL, NULL, 0);
  
  // replay graph
  cudaGraphLaunch(nothingExecutableGraph, stream);

  cudaDeviceSynchronize();
  
}
