#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "cudaExchange.h"

/*
 mpicxx.mpich -I. -c -o mpiMain.o mpiMain.cpp 
 nvcc -I. -arch=sm_60 -c -o cudaExchange.o cudaExchange.cu
 mpicxx.mpich -o mpiMain mpiMain.o  cudaExchange.o -L/usr/local/cuda-11.0/lib64 -lcuda -lcudart -lm
*/
int main(int argc, char **argv){
  
  MPI_Init(&argc, &argv);

  int rank, size;

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  printf("rank=%d, size=%d\n", rank, size);

  int N = 10;

  float *h_data = (float*) calloc(N+2, sizeof(float));  
  for(int n=0;n<N+2;++n){
    h_data[n] = rank+1;
  }

  // set up DEVICE
  cudaExchange_t *exch = cudaExchangeInit(N, rank, size, h_data);

  // copy from DEVICE to HOST
  cudaExchangeExtract(exch);

  int tag = 999;
  MPI_Status statusesOut[2], statusesIn[2];
  MPI_Request requestsOut[2]; // 2 = max messages
  MPI_Request requestsIn[2]; // 2 = max messages

  float *bufOut = exch->h_dataOut;
  for(int msgOut=0;msgOut<exch->NmessagesOut;++msgOut){
    // non-blocking point to point 
    int dest = exch->h_rankOut[msgOut];
    int bufN = exch->h_NOut[msgOut];

    MPI_Isend(bufOut, bufN, MPI_FLOAT, dest, tag, MPI_COMM_WORLD, requestsOut+msgOut);

    bufOut += bufN;
  }

  float *bufIn = exch->h_dataIn;
  for(int msgIn=0;msgIn<exch->NmessagesIn;++msgIn){
    // non-blocking point to point 
    int dest = exch->h_rankIn[msgIn];
    int bufN = exch->h_NIn[msgIn];

    MPI_Irecv(bufIn, bufN, MPI_FLOAT, dest, tag, MPI_COMM_WORLD, requestsIn+msgIn);

    bufIn += bufN;
  }

  MPI_Waitall(exch->NmessagesOut, requestsOut, statusesOut);
  MPI_Waitall(exch->NmessagesIn,  requestsIn,  statusesIn);
  

  // inject halo data into host
  cudaExchangeInject(exch);
  
  // copy back to host
  cudaExchangeToHost(exch, h_data);

  for(int r=0;r<size;++r){
    MPI_Barrier(MPI_COMM_WORLD);
    if(r==rank){

      
      for(int n=0;n<N+2;++n){
	if(n==0 || n==N+1) printf("(HALO) ");
	printf("rank: %d got %f\n", r, h_data[n]);
      }
      fflush(stdout);
      usleep(1000);
    }
  }
  

  
  MPI_Finalize();
  exit(0);
}
