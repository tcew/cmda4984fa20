function HW06WaveSolver(N)

xmin = -1;
xmax = +1;
ymin = -1;
ymax = +1;
finalTime = 50;

h = min((xmax-xmin)/(N+1), (ymax-ymin)/(N+1));

x = zeros(N+2,N+2);
y = zeros(N+2,N+2);
for i=1:N+2
  for j=1:N+2	
    x(i,j) = xmin + h*(i-1);
    y(i,j) = xmin + h*(j-1);
  end
end


dt = 0.25*h;

Nsteps = ceil(finalTime/dt);
Nsteps = 2*ceil(Nsteps/2)

dt = finalTime/Nsteps
lambda = (dt/h)^2;

alpha = 70;

xo = .1;
yo = .05;
r = sqrt((x-xo).^2+(y-yo).^2);
ids = find(r==0);
rinv = 1./r;
rinv(ids) = 0;

u = zeros(N+2,N+2); % t=0
v = 0.2*(exp(-alpha*(r+dt).^2)-...
         exp(-alpha*(r-dt).^2)).*rinv; % t=-dt

clf
hold on
for m=0:2:Nsteps-1
    
    v = update(N, lambda, u, v);
    u = update(N, lambda, v, u);   
    
    if(mod(m,20)==0)
        clf
        surf(x,y,u);
        shading interp
        axis([-1 1 -1 1 -1  3])      
        lighting gouraud 
        camlight 
        drawnow
        
        pause(.005)
    end
end
checksum = h*h*norm(u)

return
end

function v = update(N,lambda, u,uold)

idC = 1 + (1:N);
idL = (1:N);
idR = 2 + (1:N);

uW = u(idL,idC);
uC = u(idC,idC);
uE = u(idR,idC);
uS = u(idC,idL);
uN = u(idC,idR);

uoldC = uold(idC,idC);
v = uold;
v(idC,idC) = 2*uC - uoldC + lambda*(uW-2*uC+uE) + lambda*(uN-2*uC+uS);

return
end
