#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// to compile in  serial: g++ -O3 -o HW05WaveSolver HW05WaveSolver.cpp
// to run reference test: ./HW05WaveSolver  50000 -1.0 1.0 2.1

// use second order finite difference in time and space to update solution at entry n
void update(int N, float lambda, float *h_u, float *h_v){

  for(int j=1;j<=N;++j){
    for(int i=1;i<=N;++i){
      int ijC = i + j*(N+2);
      int ijN = i + (j+1)*(N+2);
      int ijS = i + (j-1)*(N+2);
      int ijE = i+1 + j*(N+2);
      int ijW = i-1 + j*(N+2);

      float uCold = h_v[ijC];
      float uC    = h_u[ijC];
      
      h_v[ijC] = 2.*uC - uCold
	+ lambda*(h_u[ijE]-2.*uC+h_u[ijW])
	+ lambda*(h_u[ijN]-2.*uC+h_u[ijS]);
    }
  }
}
  
  // initialize finite difference nodes
void setup(int N,
	   float xmin, float xmax,
	   float ymin, float ymax,
	   float finalTime,
	   float **x, float **y,
	   float **u, float **v,
	   float *dt, float *h){

  int Nall = (N+2)*(N+2);
  
  // initialize storage (adding boundary nodes at either end)
  float *h_x = (float*) calloc(Nall, sizeof(float));
  float *h_y = (float*) calloc(Nall, sizeof(float));
  float *h_u = (float*) calloc(Nall, sizeof(float));
  float *h_v = (float*) calloc(Nall, sizeof(float));

  *h = (xmax-xmin)/(N+1);
  printf("h=%17.15le\n", *h);

  // choose stable time step
  *dt = 0.25*(*h);
  
  // adjust dt to take an even integer number of steps
  int Nsteps = ceil(finalTime/(*dt));
  Nsteps = 2*ceil(Nsteps/2.);
  *dt = finalTime/Nsteps;
  
  // initialize coordinates, boundary conditions, and initial conditions
  for(int j=0;j<N+2;++j){
    for(int i=0;i<N+2;++i){
      // linear index of node (i,j)
      int ij = i + j*(N+2);
      // x-coordinate of node 
      h_x[ij] = xmin + (*h)*i;
      // y-coordinate of node 
      h_y[ij] = ymin + (*h)*j;
      // solution at time 0 at node ij
      h_u[ij] = 0;
      // compute distance of node from center of pulse
      float xo = .1;
      float yo = .05;
      float rij = sqrt(pow(h_x[ij]-xo,2)+pow(h_y[ij]-yo,2));
      float invrij = (rij) ? 1./rij:0;
      
      // solution at time -dt at node n
      float alpha = 140;
      h_u[ij] = 0;
      h_v[ij] = 0.2*(exp(-alpha*pow(rij+(*dt),2)) -
		     exp(-alpha*pow(rij-(*dt),2)))*invrij; 
    }
  }
  
  // pass pointers to calling function
  *x = h_x;
  *y = h_y;
  *u = h_u;
  *v = h_v;

}

float checksum(int N, float *h_u){

  float sum = 0;
  for(int j=0;j<N+2;++j){
    for(int i=0;i<N+2;++i){
      int ij = i + j*(N+2);
      sum += h_u[ij]*h_u[ij];
    }
  }
  return sqrt(sum);
}



// time step
void run(int N, float finalTime, float dt, float h, float *h_u, float *h_v){

  int Nsteps = finalTime/dt;

  printf("Nsteps=%d\n", Nsteps);
  printf("dt=%17.15le\n", dt);
  
  float lambda = pow(dt/h,2);

  // update recurrence formula twice
  for(int m=0;m<Nsteps;m+=2){

    // first update from u to v 
    update(N, lambda, h_u, h_v);

    // second update from v to u
    update(N, lambda, h_v, h_u);

    if((m%10)==0){ // checkpoint
      float l2norm = h*h*checksum(N, h_u);
      printf("step: %d, solution norm: %e\n", m+2, l2norm);
    }
  }

}


void save(const char *fname, int N, float *h_x, float *h_y, float *h_u){

  FILE *fp = fopen(fname, "w");

  for(int j=0;j<N+2;++j){
    for(int i=0;i<N+2;++i){
      int ij = i + j*(N+2);
      fprintf(fp, "%f %f %f\n", h_x[ij], h_y[ij], h_u[ij]);
    }
  }
  
  fclose(fp);
}

// Purpose: solve the linear second order wave equation using a second order finite difference
// in one spatial dimension using a domain [xmin,xmax] with Dirichlet boundary conditions at
// the boundary
int main(int argc, char **argv){

  // read arguments from command line
  if(argc<5) { printf("usage: ./HW05WaveSolver N xmin xmax finalTime\n"); exit(-1); }

  int   N = atoi(argv[1]); // number of points (N+2) total
  float xmin = atof(argv[2]); // x coordinate of left boundary
  float xmax = atof(argv[3]); // x coordinate of right boundary
  float ymin = xmin; // duplicate x range to y range
  float ymax = xmax;
  float finalTime = atof(argv[4]); 

  // set up finite difference nodes and initial solution
  float *h_x, *h_y, *h_u, *h_v;
  float h, dt;
  setup(N, xmin, xmax, ymin, ymax, finalTime, &h_x, &h_y, &h_u, &h_v, &dt, &h);

  // run solver
  run(N, finalTime, dt, h, h_u, h_v);

  // print solution at final time
  save("solution.dat", N, h_x, h_y, h_u);

  // compute checksum
  float sum = h*h*checksum(N, h_u);
  printf("final checksum=%17.15le\n", sum);
  
  // clean up
  free(h_x); free(h_y); free(h_u); free(h_v);
  
  return 0;
}

  
