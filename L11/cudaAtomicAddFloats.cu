
#include <stdio.h>
#include <stdlib.h>
#include "cuda.h"

// to compile on pascal node with debugging:
// nvcc -arch=sm_61 -o cudaAtomicAddFloats cudaAtomicAddFloats.cu

// thread ops:
// increment counter using atomic add operation (32 bit floats)


__global__ void atomicAddKernel(int N, float *c_a, float *c_suma){

  
  // find index of thread relative to thread-block
  int t = threadIdx.x;
    
  // find index of thread-block
  int b = blockIdx.x;

  // find block size
  int B = blockDim.x;

  // map from thread indices to array index
  int n = t + b*B;
  
  // use uninterruptable atomic add to increment counter
  if(n<N){
    float an = c_a[n];
    
    atomicAdd(c_suma, an);
  }
}


int main(int argc, char **argv){

  // 1. allocate HOST array
  int N = atoi(argv[1]);
  
  float *h_a    = (float*) calloc(N, sizeof(float));
  float *h_suma = (float*) calloc(1, sizeof(float));

  srand48(12345);
  
  h_suma[0] = 0;
  for(int n=0;n<N;++n){
    h_a[n] = drand48();
  }
  
  // 2. allocate DEVICE array
  float *c_a, *c_suma;
  cudaMalloc(&c_a,    N*sizeof(float));
  cudaMalloc(&c_suma, 1*sizeof(float));

  cudaMemcpy(c_a,  h_a,  N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_suma, h_suma, 1*sizeof(float), cudaMemcpyHostToDevice);
  
  // 3. launch DEVICE collaborate kernel
  int T = 256;
  dim3 G( (N+T-1)/T ); // number of thread blocks to use
  dim3 B( T );

  atomicAddKernel <<< G,B >>> (N, c_a, c_suma);

  
  // 4. copy data from DEVICE array to HOST array
  cudaMemcpy(h_suma, c_suma, 1*sizeof(float), cudaMemcpyDeviceToHost); 

  // 5. print out values on HOST
  printf("suma = %12.11f\n", h_suma[0]);
  
  // 6. free arrays
  cudaFree(c_a); cudaFree(c_suma);
  free(h_a); free(h_suma);

  return 0;
}
    
