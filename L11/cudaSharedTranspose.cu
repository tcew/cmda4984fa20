
#include <stdio.h>
#include <stdlib.h>
#include "cuda.h"

// to compile on pascal node with debugging:
// nvcc -g -G -arch=sm_61 -o cudaThreadCommsViaDeviceMemory cudaThreadCommsViaDeviceMemory.cu

// thread ops:
// 1. read from c_a[t + B*b]
// 2. write block reversed c_scratch[B-1-t + B*b]
// 3. read from scratch

#define BLOCK_SIZE 32

#define TX BLOCK_SIZE
#define TY 4


__global__ void transposeKernel(int Nrows, int Ncols, int *c_A, int *c_AT){

  volatile __shared__ int s_tmpA[BLOCK_SIZE][BLOCK_SIZE+1];
  
  // find index of thread relative to thread-block
  int t = threadIdx.x;
    
  // find index of thread-block
  int bx = blockIdx.x;
  int by = blockIdx.y;
  
  // load 32 row vals per thread
  {
    int col = by*BLOCK_SIZE + t;
    
#pragma unroll 32
    for(int m=0;m<BLOCK_SIZE;++m){
      int row = bx*BLOCK_SIZE + m;
      s_tmpA[m][t] = c_A[row*Ncols + col];
    }
  }

  __syncwarp();

  // store  32 row vals per thread
  {
    int col = bx*BLOCK_SIZE + t;

#pragma unroll 32
    for(int m=0;m<BLOCK_SIZE;++m){
      int row = by*BLOCK_SIZE + m;
      c_AT[row*Ncols + col] = s_tmpA[t][m];
    }
  }
}

__global__ void transposeKernelV2(int Nrows, int Ncols, int *c_A, int *c_AT){

  volatile __shared__ int s_tmpA[BLOCK_SIZE][BLOCK_SIZE+1];
  
  // find index of thread relative to thread-block
  int tx = threadIdx.x;
  int ty = threadIdx.y;
    
  // find index of thread-block
  int bx = blockIdx.x;
  int by = blockIdx.y;
  
  // load 32/TY row vals per thread
  {
    int col = by*BLOCK_SIZE + tx;
    
    for(int m=ty;m<BLOCK_SIZE;m+=TY){
      int row = bx*BLOCK_SIZE + m;
      s_tmpA[m][tx] = c_A[row*Ncols + col];
    }
  }

  __syncthreads();

  // store  32 row vals per thread
  {
    int col = bx*BLOCK_SIZE + tx;

    for(int m=ty;m<BLOCK_SIZE;m+=TY){
      int row = by*BLOCK_SIZE + m;
      c_AT[row*Ncols + col] = s_tmpA[tx][m];
    }
  }
}



int main(int argc, char **argv){

  // 1. allocate HOST array
  int Nrows = 5120, Ncols = 5120;
  //int Nrows = 256, Ncols = 256;
  int *h_A  = (int*) calloc(Nrows*Ncols, sizeof(int));
  int *h_AT = (int*) calloc(Nrows*Ncols, sizeof(int));

  for(int r=0;r<Nrows;++r){
    for(int c=0;c<Ncols;++c){
      h_A[r*Ncols+c] = c;
    }
  }
  
  // 2. allocate DEVICE array
  int *c_A, *c_AT;
  cudaMalloc(&c_A,  Nrows*Ncols*sizeof(int));
  cudaMalloc(&c_AT, Nrows*Ncols*sizeof(int));

  cudaMemcpy(c_A,  h_A,  Nrows*Ncols*sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(c_AT, h_AT, Nrows*Ncols*sizeof(int), cudaMemcpyHostToDevice);
  
  // 3. launch DEVICE collaborate kernel
#if 1
  dim3 G( (Ncols+TX-1)/TX, (Nrows+TX-1)/TX ); // number of thread blocks to use
  dim3 B(TX);
  
  transposeKernel <<< G,B >>> (Nrows, Ncols, c_A, c_AT);
#else
  dim3 G( (Ncols+TX-1)/TX, (Nrows+TX-1)/TX ); // number of thread blocks to use
  dim3 B(TX, TY);

  transposeKernelV2 <<< G,B >>> (Nrows, Ncols, c_A, c_AT);
#endif
  
  // 4. copy data from DEVICE array to HOST array
  cudaMemcpy(h_AT, c_AT, Nrows*Ncols*sizeof(int), cudaMemcpyDeviceToHost); 

#if 0
  // 5. print out values on HOST
  for(int r=0;r<Ncols;++r){
    for(int c=0;c<Nrows;++c){
      printf("%d ", h_AT[r*Nrows + c]);
    }
    printf("\n");
  }
#endif
  
  // 6. free arrays
  cudaFree(c_A); cudaFree(c_AT);
  free(h_A); free(h_AT);

  return 0;
}
    
