// https://stackoverflow.com/questions/52831987/how-to-use-wmma-functions

#include <cuda.h>
#include <cuda_fp16.h>
#include <mma.h>
#include <iostream>

using namespace nvcuda;

#define DIM 16
#define DIM2 (DIM*DIM)

__global__ void wmma_ker(float *a, float *b, float *c) {

  int e = blockIdx.x;
  int t = threadIdx.x;
  
  // use same A for all thread-blocks, different B and C
  float *ae = a;
  float *be = b + DIM2*e;
  float *ce = c + DIM2*e;
  
  __shared__ half s_a[DIM2], s_b[DIM2];
  
  // load and convert to half precision
  s_a[t] = __float2half(ae[t]);
  s_b[t] = __float2half(be[t]);
  
  __syncthreads();
  
  // Declare the fragments
  wmma::fragment<wmma::matrix_a, DIM, DIM, DIM, half, wmma::col_major> ae_frag;
  wmma::fragment<wmma::matrix_b, DIM, DIM, DIM, half, wmma::row_major> be_frag;
  wmma::fragment<wmma::accumulator, DIM, DIM, DIM, float> ce_frag;
  
  // Initialize the output to zero
  wmma::fill_fragment(ce_frag, 0.0f);

  // Load the inputs
  wmma::load_matrix_sync(ae_frag, s_a, DIM);
  wmma::load_matrix_sync(be_frag, s_b, DIM);
  
  // Perform the matrix multiplication
  wmma::mma_sync(ce_frag, ae_frag, be_frag, ce_frag);

  // Store the output
  wmma::store_matrix_sync(ce, ce_frag, DIM, wmma::mem_row_major);
}

int main(){

  int E = 500000;
  
  float *d_a, *h_a, *d_b, *h_b, *d_c, *h_c;
  
  h_c = new float[DIM*DIM*E];

  h_b = new float[DIM*DIM*E];
  h_a = new float[DIM*DIM*E];

  cudaMalloc(&d_a, DIM*DIM*E*sizeof(float));
  cudaMalloc(&d_b, DIM*DIM*E*sizeof(float));
  cudaMalloc(&d_c, DIM*DIM*E*sizeof(float));

  for (int i = 0; i < DIM*E; i++){
    for(int j = 0; j < DIM; ++j){
      h_a[i*DIM+j] = 2.0f;
      h_b[i*DIM+j] = (float)j;
    }
  }
  
  cudaMemcpy(d_a, h_a, DIM*DIM*E*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_b, h_b, DIM*DIM*E*sizeof(float), cudaMemcpyHostToDevice);

  wmma_ker<<<E,32>>>(d_a, d_b, d_c);
  
  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaEventRecord(tic);
  
  wmma_ker<<<E,32>>>(d_a, d_b, d_c);

  cudaEventRecord(toc);
  cudaDeviceSynchronize();

  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.;
  
  cudaMemcpy(h_c, d_c, E*DIM*DIM*sizeof(float), cudaMemcpyDeviceToHost);

  unsigned long long int flops = E*(unsigned long long int)DIM*DIM*DIM*2;
  
  double flopsThroughput = (flops/1.e9)/elapsed;
  
  printf("flops=%lld\n", flops);
  printf("flops/s =%e\n", flopsThroughput);
  
#if 0
  for(int e=0;e<E;++e){
    for (int i = 0; i < DIM; i++){
      for(int j = 0; j < DIM; ++j){
	std::cout << h_c[e*DIM*DIM+ i*DIM + j];
	if(j<15) std::cout << ",";
      }
      std::cout << std::endl;
    }
  }
#endif
}
