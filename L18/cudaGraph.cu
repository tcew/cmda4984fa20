#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
//#include <cuda_runtime.h>

// to compile (must use recent CUDA version):
// nvcc -arch=sm_60 -o cudaGraph cudaGraph.cu 

__global__ void nothingKernel(){

}

int main(int argc, char **argv){

  // events for timing
  cudaEvent_t start, end;
  
  // create stream for recording
  cudaStream_t stream;
  
  // cuda stream capture sequence for nothingKernel
  cudaGraph_t nothingGraph;

  cudaStreamCreate(&stream);

  cudaEventCreate(&start);
  cudaEventCreate(&end);
  
  cudaStreamBeginCapture(stream, cudaStreamCaptureModeGlobal);

  int Ntests = 100;
  for(int test=0;test<Ntests;++test){
    nothingKernel <<< 1, 1, 0, stream >>> ();
  }
  
  cudaStreamEndCapture(stream, &nothingGraph);
  
  // time graph sequence for nothing
  cudaGraphExec_t nothingInstance;
  cudaGraphInstantiate(&nothingInstance, nothingGraph, NULL, NULL, 0);
  
  cudaEventRecord(start, stream);

  // replay graph
  cudaGraphLaunch(nothingInstance, stream);

  cudaEventRecord(end, stream);

  cudaDeviceSynchronize();
  
  float elapsed;
  cudaEventElapsedTime(&elapsed, start, end);
  printf("elapsed = %g s\n", elapsed/1000.);
  
}
