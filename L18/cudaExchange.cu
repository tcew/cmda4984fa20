#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

#include "cudaExchange.h"


cudaExchange_t *cudaExchangeInit(int N, int rank, int size, float *h_data){

  cudaExchange_t *exch = (cudaExchange_t*) calloc(1, sizeof(cudaExchange_t));

  exch->N = N;
  
  cudaMalloc(&(exch->c_data), (N+2)*sizeof(float));
  cudaMemcpy(exch->c_data, h_data, (N+2)*sizeof(float), cudaMemcpyHostToDevice);

  int NmessagesOut = (rank>0 || rank<size-1) ? 2:1;
  int NmessagesIn  = (rank>0 || rank<size-1) ? 2:1;
  
  int *h_indexOut = (int*) calloc(NmessagesOut, sizeof(int));
  int *h_indexIn  = (int*) calloc(NmessagesIn,  sizeof(int));

  int outSize = 1, inSize = 1;
  int Nout = NmessagesOut*outSize;
  int Nin  = NmessagesIn*inSize;
  
  // HOST outgoing halo buffer
  exch->h_dataOut = (float*) malloc(Nout*sizeof(float));
  exch->h_dataIn  = (float*) malloc(Nin*sizeof(float));

  // message destinations (max 2 destinations)
  exch->h_rankOut = (int*) calloc(NmessagesOut, sizeof(int));
  exch->h_NOut = (int*) calloc(NmessagesOut, sizeof(int));
  exch->h_rankIn = (int*) calloc(NmessagesIn, sizeof(int));
  exch->h_NIn = (int*) calloc(NmessagesIn, sizeof(int));

  int msgOut = 0, msgIn = 0;
  int cntOut = 0, cntIn = 0;

  if(rank>0){
    // send message down
    exch->h_rankOut[msgOut] = rank-1;
    exch->h_NOut[msgOut] = 1; // one data point out

    h_indexOut[cntOut] = 1;

    ++cntOut;
    ++msgOut;
  }
  
  if(rank<size-1){
    // send message up
    exch->h_rankOut[msgOut] = rank+1;
    exch->h_NOut[msgOut] = 1; // one data point out

    h_indexOut[cntOut] = N;
  
    ++cntOut;
    ++msgOut;
  }

  if(rank>0){
    // recv message up
    exch->h_rankIn[msgIn] = rank-1;
    exch->h_NIn[msgIn] = 1; // one data point ou

    h_indexIn[cntIn] = 0;
    
    ++cntIn;
    ++msgIn;
  }

  if(rank<size-1){
    // recv message down
    exch->h_rankIn[msgIn] = rank+1;
    exch->h_NIn[msgIn] = 1; // one data point out

    h_indexIn[cntIn] = N+1;
    
    ++cntIn;
    ++msgIn;
  }

  exch->NmessagesOut = msgOut;
  exch->NmessagesIn = msgIn;

  exch->Nout = cntOut; 
  exch->Nin = cntIn; // TW: note this is the same as messages out  in 1D

  
  cudaMalloc(&(exch->c_indexOut), cntOut*sizeof(int));
  cudaMalloc(&(exch->c_indexIn), cntIn*sizeof(int));

  cudaMemcpy(exch->c_indexOut, h_indexOut, cntOut*sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(exch->c_indexIn, h_indexIn, cntOut*sizeof(int), cudaMemcpyHostToDevice);

  // DEVICE outgoing halo buffer
  cudaMalloc(&(exch->c_dataOut), cntOut*sizeof(float));
  cudaMalloc(&(exch->c_dataIn), cntIn*sizeof(float));

  return exch;
}

__global__ void extractHaloKernel(int Nout, int *c_indexOut,
				  float *c_data, float *c_dataOut){

  
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<Nout){
    int id = c_indexOut[n];
    c_dataOut[n] = c_data[id];
  }
    
}

void cudaExchangeExtract(cudaExchange_t *exch){

  int Nout = exch->Nout;
  int T = 256;
  dim3 G((Nout+T-1)/T);
  dim3 B(T);

  extractHaloKernel <<< G, B >>> (Nout,
				  exch->c_indexOut,
				  exch->c_data,
				  exch->c_dataOut);

  cudaMemcpy(exch->h_dataOut, exch->c_dataOut, Nout*sizeof(float),
	     cudaMemcpyDeviceToHost);
}


__global__ void injectHaloKernel(int Nout, int *c_indexIn,
				 float *c_dataIn, float *c_data){

  
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<Nout){
    int id = c_indexIn[n];
    c_data[id] = c_dataIn[n];
  }
    
}

void cudaExchangeInject(cudaExchange_t *exch){

  int Nout = exch->Nout;
  int T = 256;
  dim3 G((Nout+T-1)/T);
  dim3 B(T);

  cudaMemcpy(exch->c_dataIn, exch->h_dataIn, Nout*sizeof(float),
	     cudaMemcpyHostToDevice);
  
  injectHaloKernel <<< G, B >>> (Nout,
				 exch->c_indexIn,
				 exch->c_dataIn,
				 exch->c_data);

}

void cudaExchangeToHost(cudaExchange_t *exch, float *h_data){
     
  cudaMemcpy(h_data, exch->c_data, sizeof(float)*(exch->N+2), cudaMemcpyDeviceToHost);
}
