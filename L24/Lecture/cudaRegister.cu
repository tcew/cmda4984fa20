
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

__global__ void registerKernel(int N, float *c_x, float *c_y){

  int id = threadIdx.x + blockIdx.x*blockDim.x;

#if 1
  float r_v[Nv];

  if(id<N){
    for(int n=0;n<Nv;++n){
      r_v[n] = c_x[id+N*n];
    }
  }

  // res = \sum_n \sum_m (v_n v_m)
  // res = (\sum_n  v_n) (\sum_m v_m)
  float res = 0;
  for(int n=0;n<Nv;++n){
    for(int m=0;m<Nv;++m){
      res += r_v[n]*r_v[m];
    }
  }
#endif

#if 0
  // smarter way to do this
  float res = 0;
  if(id<N){
    float res1 = 0;
    for(int n=0;n<Nv;++n){
      res1 += c_x[id+N*n];
    }
    res = res1*res1;
  }
#endif
  
  if(id<N){
    c_y[id] = res;
  }
  
}

int main(int argc, char **argv){

  if(argc!=2) { printf("usage: ./cudaRegister N\n"); exit(-1); }

  int N = atoi(argv[1]);

  float *h_x = (float*) calloc(N*Nv, sizeof(float));
  float *h_y = (float*) calloc(N, sizeof(float));

  for(int n=0;n<Nv*N;++n){
    h_x[n] = 1;
  }
  
  float *c_x, *c_y;
  cudaMalloc(&c_x, N*Nv*sizeof(float));
  cudaMalloc(&c_y, N*sizeof(float));

  cudaMemcpy(c_x, h_x, N*Nv*sizeof(float), cudaMemcpyHostToDevice);

  int T = 256;
  dim3 B(T);
  dim3 G( (N+T-1)/T);
  
  registerKernel <<< G, B >>> (N, c_x, c_y);

  cudaMemcpy(h_y, c_y, N*sizeof(float), cudaMemcpyDeviceToHost);

  for(int n=0;n<N;++n){
    if(n<10){
      printf("h_y[%d] = %f\n", n, h_y[n]);
    }
  }
}
