#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

#ifndef Nv
#define Nv 10
#endif

__global__ void fooKernel(int N, float *x, float *y){

  float v[Nv];
  
  int id = threadIdx.x + blockIdx.x*blockDim.x;

  if(id<N){
    for(int n=0;n<Nv;++n){
      v[n] = x[id + n*N];
    }
  }

  float res = 0;
  for(int n=0;n<Nv;++n){
    for(int m=0;m<Nv;++m){
      res += v[n]*v[m];
    }
  }

  if(id<N){
    y[id] = res;
  }
}

int main(int argc, char **argv){

  if(argc!=2){ printf("usage: ./cudaRegister N\n"); exit(-1); }
  
  int N = atoi(argv[1]);
  
  float *h_x = (float*) calloc(Nv*N, sizeof(float));
  float *h_y = (float*) calloc(N, sizeof(float));

  for(int n=0;n<N*Nv;++n){
    h_x[n] = 1.0;
  }

  float *c_x, *c_y;
  cudaMalloc(&c_x, Nv*N*sizeof(float));
  cudaMalloc(&c_y, N*sizeof(float));

  cudaMemcpy(c_x, h_x, Nv*N*sizeof(float), cudaMemcpyHostToDevice);

  int T = 256;
  dim3 B(T);
  dim3 G( (N+T-1)/T);
  fooKernel <<<G,B>>> (N, c_x, c_y);

  cudaMemcpy(h_y, c_y, N*sizeof(float), cudaMemcpyDeviceToHost);

  for(int n=0;n<N;++n){
    if(n>10) break;
    printf("h_y[%d]=%f\n", n, h_y[n]);
  }
  
  cudaDeviceSynchronize();

  free(h_x);
  free(h_y);
  cudaFree(c_x);
  cudaFree(c_y);
  return 0;
}
