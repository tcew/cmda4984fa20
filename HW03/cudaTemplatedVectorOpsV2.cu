#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "cuda.h"
#include "leastSquaresFit.h"

// to compile on pascal node:
// nvcc -I. -arch=sm_61 -o cudaTemplatedVectorOpsV2 cudaTemplatedVectorOpsV2.cu leastSquaresFit.cu

// ftype stands for float type
template <class ftype, int opcode>
__global__ void opKernel(int N, ftype a, ftype * c_x, ftype b, ftype * c_y, ftype * c_z){

  // find index of thread relative to thread-block
  int t = threadIdx.x;

  // find index of thread-block
  int blk = blockIdx.x;

  // find number of threads in thread-block
  int B = blockDim.x;

  // construct map from thread and thread-block indices into linear array index
  int n = t + blk*B;
  
  // check index is in range
  if(n<N){
    switch(opcode){
    case  1: { ftype c = c_x[n]; if(c==123.456) c_z[n] = 1; break; } // read (condtional branch never enters)
    case  2: { c_y[n] = c_x[n];                break; } // copy
    case  3: { c_y[n] = c_x[N-1-n];            break; } // reverse
    case  4: { c_z[n] = c_x[n] + c_y[n];       break; } // add
    case  5: { c_z[n] = a*c_x[n] + b*c_y[n];   break; } // axpby
    case  6: { c_z[n] = c_x[n]/c_y[n];         break; } // division
    case  7: { c_y[n] = sin(c_x[n]);           break; } // sine
    case  8: { c_y[n] = tan(c_x[n]);           break; } // tangent
    case  9: { c_z[n] = atan2(c_y[n], c_x[n]); break; } // arc-tangent
    case 10: { c_y[n] = log(c_x[n]);           break; } // logaritm
    case 11: { c_y[n] = exp(c_x[n]);           break; } // exponential
    case 12: { c_y[n] = fabs(c_x[n]);          break; } // absolute value
    case 13: { c_z[n] = pow(c_x[n],c_y[n]);    break; } // power
    case 14: { c_y[n] = sqrt(c_x[n]);          break; } // square root
    }
  }
}

template <class ftype>
void opLaunch(int opcode, int N, ftype a, ftype * __restrict__ c_x, ftype b, ftype * __restrict__ c_y, ftype * __restrict__ c_z){

  int T = 256; // number of threads per thread block 

  dim3 G( (N+T-1)/T ); // number of thread blocks to use
  dim3 B(T);
  
  switch(opcode){
  case  1: opKernel<ftype, 1> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  case  2: opKernel<ftype, 2> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  case  3: opKernel<ftype, 3> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  case  4: opKernel<ftype, 4> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  case  5: opKernel<ftype, 5> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  case  6: opKernel<ftype, 6> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  case  7: opKernel<ftype, 7> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  case  8: opKernel<ftype, 8> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  case  9: opKernel<ftype, 9> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  case 10: opKernel<ftype,10> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  case 11: opKernel<ftype,11> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  case 12: opKernel<ftype,12> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  case 13: opKernel<ftype,13> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  case 14: opKernel<ftype,14> <<<G,B>>>(N, a, c_x, b, c_y, c_z); break;
  }
}

template <class ftype>
void runTests(int argc, char **argv, const char *type){

  long long int dataMoved[15] = {1,1,2,2,3,3,3,2,2,3,2,2,2,3,2};
  
  // 0. run with: ./cudaVectorOps 100
  if(argc!=4){ printf("usage: cudaTemplatedVectorOpsV2 Nstart Nend Ntests\n"); exit(-1); }
  
  // 1. allocate HOST array
  int Nstart = atoi(argv[1]);
  int Nend   = atoi(argv[2]);
  int Ntests = atoi(argv[3]);

  ftype *h_x = (ftype*) calloc(Nend, sizeof(ftype));
  ftype *h_y = (ftype*) calloc(Nend, sizeof(ftype));
  ftype *h_z = (ftype*) calloc(Nend, sizeof(ftype));

  for(int n=0;n<Nend;++n){
    h_x[n] = drand48();
    h_y[n] = drand48();
    h_z[n] = drand48();
  }
  
  // 2. allocate DEVICE array
  ftype *c_x, *c_y, *c_z;
  cudaMalloc(&c_x, Nend*sizeof(ftype));
  cudaMalloc(&c_y, Nend*sizeof(ftype));
  cudaMalloc(&c_z, Nend*sizeof(ftype));

  // 3. copy data to DEVICE arrays
  cudaMemcpy(c_x, h_x, Nend*sizeof(ftype), cudaMemcpyHostToDevice);
  cudaMemcpy(c_y, h_y, Nend*sizeof(ftype), cudaMemcpyHostToDevice);
  cudaMemcpy(c_z, h_z, Nend*sizeof(ftype), cudaMemcpyHostToDevice);
  
  // 4. constants
  ftype a = 1.0, b = 2.0;
  
  // 5. warm up DEVICE kernels
  int Nops = 14;
  for(int opcode=1;opcode<=Nops;++opcode){
    opLaunch<ftype> (opcode, Nend, a, c_x, b, c_y, c_z);
  }

  cudaDeviceSynchronize();

  // 6. run timing tests
  cudaEvent_t tic, toc;
  
  double *T = (double*) calloc(Ntests, sizeof(double));
  double *GB = (double*) calloc(Ntests, sizeof(double));
  double *coeffs = (double*) calloc(2, sizeof(double));
  
  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  for(int opcode=1;opcode<=Nops;++opcode){

    for(int test=0;test<Ntests;++test){

      cudaDeviceSynchronize();

      int Nruns = 10;
      int N = Nstart + floor((test/(double)(Ntests-1))*(Nend-Nstart));

      cudaEventRecord(tic);
      for(int n=0;n<Nruns;++n){
	opLaunch(opcode, N, a, c_x, b, c_y, c_z);
      }
      cudaEventRecord(toc);      
      cudaDeviceSynchronize();

      // compute elapsed time
      float elapsed;
      cudaEventElapsedTime(&elapsed, tic, toc);
      elapsed /= Nruns;

      // store data
      GB[test] = dataMoved[opcode]*sizeof(ftype)*(N/1.e9); // GB
      T[test] = elapsed/1000.;
    }

    leastSquaresFit(Ntests, GB, T, coeffs);

    printf("%02d, %E, %d %%%% op, T_0 (secs), W_{max} (GB/s), %s\n", opcode, coeffs[0], (int)(1./coeffs[1]), type);

    // output to file
    char fname[BUFSIZ];
    sprintf(fname, "bench%02dbw%s.tex", opcode, type);
    FILE *fp = fopen(fname, "w");
    fprintf(fp, "\\addplot+[mark=none, thick] coordinates {\n");
    for(int test=0;test<Ntests;++test){
      fprintf(fp, "(%g, %g)\n", GB[test], GB[test]/T[test]);
    }
    fprintf(fp, "};\n");
    fclose(fp);

  }
  
  cudaFree(c_x); cudaFree(c_y); cudaFree(c_z);
  free(h_x); free(h_y); free(h_z); free(T); free(GB);

}
    
int main(int argc, char **argv){

  // run with fp32
  runTests <float> (argc, argv, "fp32");
  
  // run with f64
  runTests <double> (argc, argv, "fp64");
  
}
