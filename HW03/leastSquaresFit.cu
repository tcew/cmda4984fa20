#include <math.h>

// do a least squares fit to linear polynomial
void leastSquaresFit(int N, double *x, double *y, double *coeffs){
  
  double A[2][2] = {{0,0}, {0,0}};
  double r[2] = {0,0};

  // l2 minimize [1 x]*coeffs - y
  // solve normal equations:
  // [ 1 x]'*[1 x]*coeffs = [1 x]'*y
  
  for(int n=0;n<N;++n){
    A[0][0] += 1.0;
    A[0][1] += x[n];
    A[1][0] += x[n];
    A[1][1] += x[n]*x[n];

    r[0] += y[n];
    r[1] += x[n]*y[n];
  }

  double J = A[0][0]*A[1][1] - A[0][1]*A[1][0];

  coeffs[0] = ( A[1][1]*r[0] - A[0][1]*r[1])/J;
  coeffs[1] = (-A[1][0]*r[0] + A[0][0]*r[1])/J;
}
