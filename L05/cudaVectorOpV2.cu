#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "cuda.h"

// to compile on pascal node:
// nvcc -o cudaVectorOpV2 cudaVectorOpV2.cu -lm

__global__ void opKernel(int N,
			 double alpha, double *c_x,
			 double beta, double *c_y,
			 double *c_z){
  
  // find index of thread relative to thread-block
  int t = threadIdx.x;

  // find index of thread-block
  int b = blockIdx.x;

  // find number of threads in thread-block
  int B = blockDim.x;

  // construct map from thread and thread-block indices into linear array index
  int n = t + b*B;
  
  // check index is in range
  if(n<N){
    // zaxpby
    c_z[n] = alpha*c_x[n] + beta*c_y[n]; // work done by thread
  }
}


#define mymin(a,b) ( (a)<(b) ? (a):(b) )

int main(int argc, char **argv){

  // 0. to run with arrays of length 1024:
  // ./cudaVectorOpV1 1024
  if(argc!=2){ printf("usage: ./cudaVectorOpV1 N\n"); exit(-1);}

  cudaSetDevice(1); // choose your assigned DEVICE
  
  // 1. allocate HOST array
  int N = atoi(argv[1]);
  double *h_x = (double*) calloc(N, sizeof(double));
  double *h_y = (double*) calloc(N, sizeof(double));
  double *h_z = (double*) calloc(N, sizeof(double));

  // 2. populate HOST arrays
  for(int n=0;n<N;++n){
    h_x[n] = drand48();
    h_y[n] = drand48();
  }
  
  // 3. allocate DEVICE array
  double *c_x, *c_y, *c_z;
  cudaMalloc(&c_x, N*sizeof(double));
  cudaMalloc(&c_y, N*sizeof(double));
  cudaMalloc(&c_z, N*sizeof(double));

  // 4. copy data from HOST to DEVICE
  cudaMemcpy(c_x, h_x, N*sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(c_y, h_y, N*sizeof(double), cudaMemcpyHostToDevice);
  
  // 5. launch DEVICE fill kernel
  int T = 256; // number of threads per thread block 
  dim3 G( (N+T-1)/T ); // number of thread blocks to use
  dim3 B(T);

  double alpha = 1, beta = -1;
  
  // 6. warm up run (copy kernel code to DEVICE)
  int Nwarm = 5;
  for(int n=0;n<Nwarm;++n)
    opKernel <<< G,B >>> (N, alpha, c_x, beta, c_y, c_z);
  
  // 7. create CUDA events
  cudaEvent_t tic, toc;
  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  // 8. insert tic event into GPU queue
  cudaEventRecord(tic);

  // 9. run kernel 10 times
  int Nrun = 10;
  for(int n=0;n<Nrun;++n)
    opKernel <<< G,B >>> (N, alpha, c_x, beta, c_y, c_z);

  // 10. insert toc event into GPU queue
  cudaEventRecord(toc);

  // 11. make sure HOST and DEVICE are synced 
  cudaDeviceSynchronize();

  // 12. estimate time take
  float time;
  cudaEventElapsedTime(&time, tic, toc);
  time = time/1000; // convert to seconds
  
  // 13. estimate bytes moved
  long long int bytes = Nrun*3*N*sizeof(double);

  // 14. simple estimate of bandwidth for this problem size
  double WB = bytes/time;

  // 15. print
  printf("%lld, %g, %g %%%% B (bytes), T (s), W (GB/s)\n",
	 bytes, time, WB/1.e9);
  
  // 16. free arrays
  cudaFree(c_x); cudaFree(c_y); cudaFree(c_z);
  free(h_x); free(h_y); free(h_z);

  return 0;
}
    
