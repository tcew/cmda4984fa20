
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cuda.h>

#define mymax(a,b) (((a)>(b)) ? (a) : (b) )
#define mymin(a,b) (((a)<(b)) ? (a) : (b) )

// DEVICE CUDA KERNEL CODE
// z_n = alpha*x_n + beta*y_n
__global__ void opKernel(int N, 
			 double alpha,
			 double *c_x,
			 double beta,
			 double *c_y,
			 double *c_z){

  int t = threadIdx.x; // thread index in block
  int b = blockIdx.x;  // block index in grid
  int B = blockDim.x;  // number of threads in block
  
  int n = t + b*B; // compute the index n based on thread indexes

  if(n<N){
    c_z[n] = alpha*c_x[n] + beta*c_y[n];
  }
  
}

int main(int argc, char **argv){

  if(argc!=2){ printf("usage: ./cudaVectorOpV1 N\n"); exit(-1); }
  
  int N = atoi(argv[1]);

  // allocate arrays on the HOST heap
  double *h_x = (double*) calloc(N, sizeof(double));
  double *h_y = (double*) calloc(N, sizeof(double));
  double *h_z = (double*) calloc(N, sizeof(double));

  for(int n=0;n<N;++n){
    h_x[n] = drand48();
    h_y[n] = drand48();
  }

  // allocate arrays on the DEVICE heap
  double *c_x, *c_y, *c_z;
  cudaMalloc(&c_x, N*sizeof(double));
  cudaMalloc(&c_y, N*sizeof(double));
  cudaMalloc(&c_z, N*sizeof(double));

  // copy data from HOST arrays to DEVICE arrays
  cudaMemcpy(c_x, h_x, N*sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(c_y, h_y, N*sizeof(double), cudaMemcpyHostToDevice);

  // specify the array of threads
  // - designed the kernel to require at least N threads
  // - array of threads is multidimensional
  // - we need to specify number of thread-blocks
  // - we need to specify number of threads per thread-block

  int T = 256;
  dim3 B(T);
  dim3 G( (N+T-1)/T );

  // just queues up the kernel on the GPU
  double alpha = 1., beta = -1.2;
  opKernel <<< G, B >>> (N, alpha, c_x, beta, c_y, c_z);

  // we do not know at this point if the kernel has completed

  // when we execute a (vanilla) cudaMemcpy it waits until the
  // queue of kernels is emptied (i.e. kernels finish)
  cudaMemcpy(h_z, c_z, N*sizeof(double), cudaMemcpyDeviceToHost);

  // at this point the HOST heap array h_z contains the GPU result
  for(int n=0;n<mymin(10,N);++n){
    printf("h_z[%d]=%g\n", n, h_z[n]);
  }

  cudaFree(c_x); cudaFree(c_y); cudaFree(c_z);
  free(h_x); free(h_y); free(h_z);
  
  return 0;
  
  
}
