
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cuda.h>

#define mymax(a,b) (((a)>(b)) ? (a) : (b) )
#define mymin(a,b) (((a)<(b)) ? (a) : (b) )

// DEVICE CUDA KERNEL CODE
// z_n = alpha*x_n + beta*y_n
__global__ void opKernel(int N, 
			 double alpha,
			 double *c_x,
			 double beta,
			 double *c_y,
			 double *c_z){

  int t = threadIdx.x; // thread index in block
  int b = blockIdx.x;  // block index in grid
  int B = blockDim.x;  // number of threads in block
  
  int n = t + b*B; // compute the index n based on thread indexes

  if(n<N){
    c_z[n] = alpha*c_x[n] + beta*c_y[n];
  }
  
}

int main(int argc, char **argv){

  if(argc!=2){ printf("usage: ./cudaVectorOpV1 N\n"); exit(-1); }
  
  int N = atoi(argv[1]);

  // allocate arrays on the HOST heap
  double *h_x = (double*) calloc(N, sizeof(double));
  double *h_y = (double*) calloc(N, sizeof(double));
  double *h_z = (double*) calloc(N, sizeof(double));

  for(int n=0;n<N;++n){
    h_x[n] = drand48();
    h_y[n] = drand48();
  }

  // allocate arrays on the DEVICE heap
  double *c_x, *c_y, *c_z;
  cudaMalloc(&c_x, N*sizeof(double));
  cudaMalloc(&c_y, N*sizeof(double));
  cudaMalloc(&c_z, N*sizeof(double));

  // copy data from HOST arrays to DEVICE arrays
  cudaMemcpy(c_x, h_x, N*sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(c_y, h_y, N*sizeof(double), cudaMemcpyHostToDevice);

  // specify the array of threads
  // - designed the kernel to require at least N threads
  // - array of threads is multidimensional
  // - we need to specify number of thread-blocks
  // - we need to specify number of threads per thread-block

  int T = 256;
  dim3 B(T);
  dim3 G( (N+T-1)/T );

  double alpha = 1., beta = -1.2;
  int Nwarm = 10;
  for(int n=0;n<Nwarm;++n)
    opKernel <<< G, B >>> (N, alpha, c_x, beta, c_y, c_z);
  
  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaEventRecord(tic);
  
  // just queues up the kernel on the GPU
  int Nrun = 10;

  for(int n=0;n<Nrun;++n)
    opKernel <<< G, B >>> (N, alpha, c_x, beta, c_y, c_z);

  cudaEventRecord(toc);

  // make sure DEVICE has finished
  cudaDeviceSynchronize();
  
  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed = elapsed/1000; // translates into elapsed time in seconds
  
  // estimate the bytes moved
  long long int bytes = Nrun*3*sizeof(double)*N;

  // estimate the throughput (bytes moved / time )
  double W = (bytes/elapsed)/1.e9; // GB/s (gigabytes per second)

  printf("%g %g %g %%%% gigabytes moved, time, bandwidth (GB/s)\n",
	 bytes/1.e9, elapsed, W);


  cudaFree(c_x); cudaFree(c_y); cudaFree(c_z);
  free(h_x); free(h_y); free(h_z);
  
  return 0;
  
  
}
