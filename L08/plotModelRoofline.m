T0    = 20e-6;   %% kernel overhead estimate
Rpeak = 3.6e11; %% streaming DEVICE bw
Fpeak = 9.e12;  %% high percentage of peak DEVICE GFLOPS/s

%% grid of AI and B values
maxAI = 1.1*Fpeak/Rpeak;    %% maximum arithmetic intensity (AI)
maxB = 1e9;    %% maximum number of bytes

NAI = 200;     %% number of AI samples
NB = 200;      %% number of bytes values to sample

AI=linspace(0,maxAI,NAI)'*ones(1,NB);
B = ones(NAI,1)*10.^linspace(0, 9, NB);

%% model for execution time
TB = T0 + B.*max(1/Rpeak, AI/Fpeak);

%% model for throughput in GLOPS
GFLOPS = (B.*AI./TB)/1e9;


figure(1)
surf(AI, GFLOPS, log10(B))

view(2)
set(gca, 'FontSize', 20)
xlabel('Arithmetic intensity', 'FontSize', 20)
ylabel('Floating point throughput (GFLOPS/s)', 'FontSize', 20)
set(gcf, 'Color', 'w');
shading interp
ha = colorbar
set(get(ha, 'title'), 'String', 'log_{10}(bytes)')
colormap jet
grid on

figure(2)
surf(B, GFLOPS, AI )

view(2)
set(gca, 'FontSize', 20)
xlabel('Bytes moved', 'FontSize', 20)
ylabel('Floating point throughput (GFLOPS/s)', 'FontSize', 20)
set(gcf, 'Color', 'w');
shading interp
ha = colorbar
set(get(ha, 'title'), 'String', 'Arithmetic Intensity')
colormap jet
grid on




