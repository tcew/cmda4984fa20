// https://stackoverflow.com/questions/52831987/how-to-use-wmma-functions

#include <cuda.h>
#include <cuda_fp16.h>
#include <mma.h>
#include <iostream>

/*
  nvcc -arch=sm_70 -o cudaTensorUnitsIntro cudaTensorUnitsIntro.cu
 */

using namespace nvcuda;

#define DIM 16
#define DIM2 (DIM*DIM)

__global__ void matrixMultiplyKernel(half *c_halfA, half *c_halfB, float *c_D) {

  int e = blockIdx.x;
  int t = threadIdx.x;
  
  // use same A for all thread-blocks, different B and C
  half  *Ae = c_halfA + DIM2*e;
  half  *Be = c_halfB + DIM2*e;
  float *De = c_D + DIM2*e;
  
  // Declare the fragments
  wmma::fragment<wmma::matrix_a, DIM, DIM, DIM, half, wmma::col_major> Ae_frag;
  wmma::fragment<wmma::matrix_b, DIM, DIM, DIM, half, wmma::row_major> Be_frag;
  wmma::fragment<wmma::accumulator, DIM, DIM, DIM, float> De_frag;
  
  // Initialize the output to zero
  wmma::fill_fragment(De_frag, 0.0f);

  // Load the inputs
  wmma::load_matrix_sync(Ae_frag, Ae, DIM);
  wmma::load_matrix_sync(Be_frag, Be, DIM);
  
  // Perform the matrix multiplication
  wmma::mma_sync(De_frag, Ae_frag, Be_frag, De_frag);

  // Store the dutput
  wmma::store_matrix_sync(De, De_frag, DIM, wmma::mem_row_major);
}

__global__ void float2HalfKernel(int N, float *a, half *b){
	   
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    b[n] = __float2half(a[n]);
  }
}

void float2Half(int N, float *c_a, half *c_halfa){
  int T = 256;
  dim3 B(T);
  dim3 G((N+T-1)/T);
  
  float2HalfKernel<<<G,B>>>(N, c_a, c_halfa);
}

int main(int argc, char **argv){

  if(argc!=2) { printf("usage: ./cudaTensorUnitsIntro E\n"); exit(-1); }

  int E = atoi(argv[1]);
  
  float *h_A, *c_A;
  half *c_halfA;

  float *h_B, *c_B;
  half *c_halfB;

  float *c_D, *h_D;

  int N = DIM*DIM*E;

  h_A = new float[N];
  h_B = new float[N];
  h_D = new float[N];

  // create arrays for A,B,D
  cudaMalloc(&c_A, N*sizeof(float));
  cudaMalloc(&c_B, N*sizeof(float));
  cudaMalloc(&c_D, N*sizeof(float));

  for (int i = 0; i < DIM*E; i++){
    for(int j = 0; j < DIM; ++j){
      h_A[i*DIM+j] = 1.0f;
      h_B[i*DIM+j] = (float)j;
    }
  }

  cudaMemcpy(c_A, h_A, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_B, h_B, N*sizeof(float), cudaMemcpyHostToDevice);

  // allocate storage for half precision arrays
  cudaMalloc(&c_halfA, N*sizeof(half));
  cudaMalloc(&c_halfB, N*sizeof(half));

  // convert float to half
  float2Half(N, c_A, c_halfA);
  float2Half(N, c_B, c_halfB);

  // compute multiply of D = A*B
  matrixMultiplyKernel<<<E,32>>>(c_halfA, c_halfB, c_D);
  
  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaEventRecord(tic);
  
  matrixMultiplyKernel<<<E,32>>>(c_halfA, c_halfB, c_D);

  cudaEventRecord(toc);
  cudaDeviceSynchronize();

  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.;
  
  cudaMemcpy(h_D, c_D, N*sizeof(float), cudaMemcpyDeviceToHost);

  unsigned long long int flops = E*(unsigned long long int)DIM*DIM*DIM*2;
  
  double flopsThroughput = (flops/1.e9)/elapsed;
  
  printf("flops=%lld\n", flops);
  printf("flops/s =%e\n", flopsThroughput);
  
#if 1
  for(int e=0;e<E;++e){
    printf("warp %d: \n", e);
    for (int i = 0; i < DIM; i++){
      for(int j = 0; j < DIM; ++j){
	std::cout << h_D[e*DIM*DIM+ i*DIM + j];
	if(j<15) std::cout << ",";
      }
      std::cout << std::endl;
    }
  }
#endif
}
