#include "timer.h"

// see: http://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/
timespec getHostTime(){
  timespec t;
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t);
  return t;
}

timespec getHostElapsedTime(timespec start, timespec end){
  timespec temp;
  if ((end.tv_nsec-start.tv_nsec)<0) {
    temp.tv_sec = end.tv_sec-start.tv_sec-1;
    temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
  } else {
    temp.tv_sec = end.tv_sec-start.tv_sec;
    temp.tv_nsec = end.tv_nsec-start.tv_nsec;
  }
  return temp;
}

long getHostElapsedNanoseconds(timespec t){

  return t.tv_nsec;
}

long getHostElapsedResolution(){
  timespec t;
  clock_getres(CLOCK_PROCESS_CPUTIME_ID, &t);
  return t.tv_nsec;
}
