
#include <stdio.h>
#include <stdlib.h>
#include "cuda.h"
#include "timer.h"

// to compile: nvcc -o cudaTimingV3 cudaTimingV3.cu timer.cpp -I.
// to run: ./cudaTimingV3 1000

// fill an array with the same scalar value in each entry
__global__ void fillKernel(int N, double val, double *c_a){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int d = blockDim.x;

  // logic:
  // d is the block size
  // t is the offset in the start of block b

  int n = d*b + t;

  if(n<N)
    c_a[n] = val;
  
}


int main(int argc, char **argv){

  if(argc!=2){ printf("usage: ./cudaFillVectorLecture N\n"); exit(-1);}
  
  int N = atoi(argv[1]);

  double val = 1.2;

  double *h_a = (double*) malloc(N*sizeof(double));

  double *c_a;

  // malloc N*8 bytes in the GPU memory
  cudaMalloc(&c_a, N*sizeof(double)); // cudaMalloc will modify the pointer

  // use a CUDA kernel to fill the entries of the c_a array
  int B = 128; // number of thread per thread-block
  int G = (N+B-1)/B; // number of thread-blocks

  // "warm up" GPU
  int Nwarm = 10;
  for(int n=0;n<Nwarm;++n)
    fillKernel <<< G, B >>> (N, val, c_a);

  // create cuda events
  cudaEvent_t ticDevice, tocDevice;
  cudaEventCreate(&ticDevice);
  cudaEventCreate(&tocDevice);
  
  // insert start event recording on GPU
  cudaEventRecord(ticDevice);
  
  // queue kernel
  int Nrun = 1;
  for(int n=0;n<Nrun;++n)
    fillKernel <<< G, B >>> (N, val, c_a);

  // insert end event recording on GPU
  cudaEventRecord(tocDevice);

  // flush queue on DEVICE
  cudaDeviceSynchronize();

  // find elapsed time on DEVICE
  float elapsedDeviceMilliseconds;
  cudaEventElapsedTime(&elapsedDeviceMilliseconds, ticDevice, tocDevice); 
  
  // estimate bandwidth
  long long int bytes = Nrun*N*sizeof(double); // data written in the Nrun tests
  double T = elapsedDeviceMilliseconds/1.e3;
  double WN = bytes/T;
  printf("%e, %e, %%%% Bytes (GB), Estimated bandwidth (GB/s) \n",
	 bytes/1.e9, WN/1.e9); // write out bytes in GB and bandwidth in GB/s (billions of bytes per seconds)

  // free up CUDA arrays
  cudaFree(c_a);

  // free up HOST array
  free(h_a);

  // free events
  cudaEventDestroy(ticDevice);
  cudaEventDestroy(tocDevice);
  
}
