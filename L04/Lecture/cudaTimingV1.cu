
#include <stdio.h>
#include <stdlib.h>
#include "cuda.h"
#include "timer.h"

// to compile: nvcc -o cudaTimingV1 cudaTimingV1.cu timer.cpp -I.
// to run: ./cudaTimingV1 1000

// fill an array with the same scalar value in each entry
__global__ void fillKernel(int N, double val, double *c_a){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int d = blockDim.x;

  // logic:
  // d is the block size
  // t is the offset in the start of block b

  int n = d*b + t;

  if(n<N)
    c_a[n] = val;
  
}


int main(int argc, char **argv){

  if(argc!=2){ printf("usage: ./cudaFillVectorLecture N\n"); exit(-1);}
  
  int N = atoi(argv[1]);

  double val = 1.2;

  double *h_a = (double*) malloc(N*sizeof(double));

  double *c_a;

  // malloc N*8 bytes in the GPU memory
  cudaMalloc(&c_a, N*sizeof(double)); // cudaMalloc will modify the pointer

  // use a CUDA kernel to fill the entries of the c_a array
  int B = 128; // number of thread per thread-block
  int G = (N+B-1)/B; // number of thread-blocks

  // use high resolution system timer on HOST
  timespec tic, toc, elapsed;

  // sync HOST and DEVICE (flushes DEVICE queue)
  cudaDeviceSynchronize();
  tic = getHostTime();

  // queue kernel
  fillKernel <<< G, B >>> (N, val, c_a);

  // flush queue on DEVICE and then measure time
  cudaDeviceSynchronize();
  toc = getHostTime();

  // find elapsed time on HOST
  elapsed = getHostElapsedTime(tic, toc);

  // retrieve elapsed time in nanoseconds
  long ns = getHostElapsedNanoseconds(elapsed);
  printf("Time elapsed: %e\n", ns/1.e9);
  
  // free up CUDA arrays
  cudaFree(c_a);

  // free up HOST array
  free(h_a);
  
}
