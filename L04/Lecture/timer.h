#include <time.h>

// use high resolution timer
// see: http://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/
struct timespec;

#if 0
struct timespec {
  time_t tv_sec; /* seconds */
  long tv_nsec; /* nanoseconds */
};
#endif

timespec getHostTime();
timespec getHostElapsedTime(timespec start, timespec end);
long getHostElapsedNanoseconds(timespec t);
long getHostElapsedResolution();
