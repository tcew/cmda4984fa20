#include <stdio.h>
#include "cuda.h"

// hello world with rank output, CUDA version
// GPU kernel function (runs on DEVICE)
__global__ void helloWorldKernel(){

  // find out what the thread's rank is in thread-block
  int t = threadIdx.x; // in our example a number between [0,127]

  // find out which thread-block this thread is in
  int b = blockIdx.x; // in our example a number between [0,27]
  
  printf("Hello world from GPU (thread %d in block %d)\n", t, b);
}

// HOST main function
int main(int argc, char **argv){

  // code in here runs on the CPU (HOST)
  printf("Hello world from CPU\n");

  // HOST queues up kernel for execution on DEVICE
  int G = 28; // number of thread-blocks in Grid
  int B = 128; // number of threads in thread-Blocks
  helloWorldKernel <<< G, B >>> ();

  // HOST does not know the state of the kernel execution
  // force a synchronization between DEVICE and HOST
  cudaDeviceSynchronize();

  // at this point we know the kernel has finished
  return 0;
}
