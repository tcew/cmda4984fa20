#include <stdio.h>
#include "cuda.h"

// hello world with rank output, CUDA version
// GPU kernel function (runs on DEVICE)
__global__ void helloWorldKernel(){

  // find out what the thread's rank is in thread-block
  int tx = threadIdx.x; // x-index of thread in thread-block
  int ty = threadIdx.y; // y-index of thread in thread-block

  // find out which thread-block this thread is in
  int bx = blockIdx.x; // x-index of thread-block in grid
  int by = blockIdx.y; // y-index of thread-block in grid
  
  printf("Hello world from GPU (thread (%d,%d) in block (%d,%d))\n",
	 tx, ty, bx, by);
}

// HOST main function
int main(int argc, char **argv){

  // code in here runs on the CPU (HOST)
  printf("Hello world from CPU\n");

  // HOST queues up kernel for execution on DEVICE
  dim3 G(8,4,1); // 8x4 array of thread-blocks
  dim3 B(16,4,1); // array of threads in each thread-blocks
  helloWorldKernel <<< G, B >>> ();

  // HOST does not know the state of the kernel execution
  // force a synchronization between DEVICE and HOST
  cudaDeviceSynchronize();

  // at this point we know the kernel has finished
  return 0;
}
