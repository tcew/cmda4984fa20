#include <stdio.h>
#include <stdlib.h>
#include "cuda.h"

// to compile on pascal node:
// nvcc -arch=sm_61 -o cudaScan cudaScan.cu

#define MASK 0xffffffff

__device__ int shflWarpScan(int l, int x){

  int y = 0;
  
  // pass up and add
  y = __shfl_up_sync(MASK, x,  1); x += (l>= 1) ? y : 0;
  y = __shfl_up_sync(MASK, x,  2); x += (l>= 2) ? y : 0;
  y = __shfl_up_sync(MASK, x,  4); x += (l>= 4) ? y : 0;
  y = __shfl_up_sync(MASK, x,  8); x += (l>= 8) ? y : 0;
  y = __shfl_up_sync(MASK, x, 16); x += (l>=16) ? y : 0;

  return x;
}


__global__ void blockShflScanKernel(int N, int *c_a, int *c_scana, int *c_offsets){

  // find index of thread relative to thread-block
  int t = threadIdx.x;

  // find index of thread-block
  int b = blockIdx.x;
  
  // find lane
  int l = t%32;
  
  // find warp
  int w = t >> 5; // divides by 32

  // find number of threads in thread-block
  int B = blockDim.x;

  // construct map from thread and thread-block indices into linear array index
  int n = t + b*B;

  int x = 0;

  // check index is in range
  if(n<N){
    x = c_a[n]; // work done by thread
  }

  // warp based scan using shfl op
  x = shflWarpScan(l, x);
  
  // collect warp offsets
  __shared__ int s_offsets[32];
  
  if(l==31) s_offsets[w] = x;

  __syncthreads(); // ensure all warps have contributed offset
  
  // nominate warp 0 to scan offsets
  if(w==0){
    int offx = s_offsets[l];

    offx = shflWarpScan(l, offx);

    s_offsets[l] = offx;
  }

  __syncthreads();

  // shift by warp offset
  x += (w!=0) ? s_offsets[w-1]:0;

  if(n<N)
    c_scana[n] = x;

  if(t==B-1)
    c_offsets[b] = x;
}

__device__ int sharedWarpScan(int l, int w, int x){
  
  // pass up and add
  __shared__ int s_x[32][32];

  // step #1
  s_x[w][l] = x;

  __syncwarp();
  x += (l>=1) ? s_x[w][l-1]:0;

  // step #2
  __syncwarp();
  s_x[w][l] = x;

  __syncwarp();
  x += (l>=2) ? s_x[w][l-2]:0;

  // step #3
  __syncwarp();
  s_x[w][l] = x;
  
  __syncwarp();
  x += (l>=4) ? s_x[w][l-4]:0;

  // step #4
  __syncwarp();
  s_x[w][l] = x;

  __syncwarp();
  x += (l>=8) ? s_x[w][l-8]:0;

  // step #5
  __syncwarp();
  s_x[w][l] = x;

  __syncwarp();
  x += (l>=16) ? s_x[w][l-16]:0;
  
  return x;
}




__global__ void blockSharedScanKernel(int N, int *c_a, int *c_scana, int *c_offsets){

  // find thread index info
  int t = threadIdx.x;
  int b = blockIdx.x;
  int l = t%32; // lane index
  int w = t >> 5; // warp index
  int B = blockDim.x;
  int n = t + b*B;

  int x = 0;

  // check index is in range
  if(n<N){
    x = c_a[n]; // work done by thread
  }

  // warp based scan using shfl op
  x = sharedWarpScan(l, w, x);
  
  // collect warp offsets
  __shared__ int s_offsets[32];
  
  if(l==31) s_offsets[w] = x;

  __syncthreads(); // ensure all warps have contributed offset
  
  // nominate warp 0 to scan offsets
  if(w==0){
    int offx = s_offsets[l];

    offx = sharedWarpScan(l, w, offx);

    s_offsets[l] = offx;
  }

  __syncthreads();

  // shift by warp offset
  x += (w!=0) ? s_offsets[w-1]:0;

  if(n<N)
    c_scana[n] = x;

  if(t==B-1)
    c_offsets[b] = x;
}



__global__ void finalizeScanKernel(int N, int *c_offsets, int *c_scana){

  // find index of thread relative to thread-block
  int t = threadIdx.x;

  // find index of thread-block
  int b = blockIdx.x;

  // find number of threads in thread-block
  int B = blockDim.x;
  
  // construct map from thread and thread-block indices into linear array index
  int n = t + b*B;
  
  if(b>0)
    c_scana[n] += c_offsets[b-1];
}

int main(int argc, char **argv){


  if(argc!=2){ printf("usage; ./cudaScan N\n"); exit(-1);}
  
  // 1. allocate HOST array
  int N = atoi(argv[1]);
  int T = 1024; // number of threads per thread block
  int Noffsets = (N+T-1)/T;
  
  int *h_a = (int*) calloc(N, sizeof(int));
  int *h_scana = (int*) calloc(N, sizeof(int));
  int *h_offsets = (int*) calloc(Noffsets, sizeof(int));

  for(int n=0;n<N;++n){
    h_a[n] = 1;
    h_scana[n] = -999;
  }
  
  // 2. allocate DEVICE array
  int *c_a, *c_scana, *c_offsets;
  cudaMalloc(&c_a, N*sizeof(int));
  cudaMalloc(&c_scana, N*sizeof(int));
  cudaMalloc(&c_offsets, Noffsets*sizeof(int));

  cudaMemcpy(c_a, h_a, N*sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(c_scana, h_scana, N*sizeof(int), cudaMemcpyHostToDevice);
  
  // 3. launch DEVICE block scan kernel
  dim3 G( (N+T-1)/T ); // number of thread blocks to use
  dim3 B(T);
  
  //blockShflScanKernel <<< G,B >>> (N, c_a, c_scana, c_offsets);
  blockSharedScanKernel <<< G,B >>> (N, c_a, c_scana, c_offsets);

  // 4. copy offsets back to HOST
  cudaMemcpy(h_offsets, c_offsets, Noffsets*sizeof(int), cudaMemcpyDeviceToHost);

  // 5. scan offsets on HOST
  for(int n=1;n<Noffsets;++n){
    h_offsets[n] += h_offsets[n-1];
  }

  // 6. copy scanned offsets back to DEVCE
  cudaMemcpy(c_offsets, h_offsets, Noffsets*sizeof(int), cudaMemcpyHostToDevice);
  
  // 7. launch offsets DEVICE scan kernel
  finalizeScanKernel <<< G,B >>> (N, c_offsets, c_scana);

  // 8. copy data from DEVICE array to HOST array
  cudaMemcpy(h_scana, c_scana, N*sizeof(int), cudaMemcpyDeviceToHost); 

  // 9. print out values on HOST
  //  for(int n=0;n<N;++n) printf("h_scana[%d] = %d\n", n, h_scana[n]);
  printf("Last entry in scan: %d compared to N: %d\n", h_scana[N-1], N);

  // 10. free arrays
  cudaFree(c_a); free(h_a); return 0;
}
    
