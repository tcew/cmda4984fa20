
#include <stdio.h>
#include <stdlib.h>
#include "cuda.h"

// to compile on pascal node with debugging:
// nvcc -g -G -arch=sm_61 -o cudaShflDown cudaShflDown.cu

// thread ops:
// one thread reads a value and uses __shfl_sync to broadcast to other threads
// https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#warp-shuffle-functions

// 32 bit number with all bits set to 1
#define SHFL_MASK 0xFFFFFFFF

__global__ void shflDownWarpKernel(int N, int *c_a, int *c_res){

  
  // find index of thread relative to thread-block
  int t = threadIdx.x;
    
  // find index of thread-block
  int b = blockIdx.x;

  // find block size
  int B = blockDim.x;

  // map from thread indices to array index
  int n = t + b*B;

  // SIMD lane of thread
  int l = t%32;

  // threads initialize there register value
  int x = t;

  // threads shift down by 4
  int offset = 4;
  int y = __shfl_down_sync(SHFL_MASK, x, offset);

  if(n<N)
    c_res[n] = y;
}


int main(int argc, char **argv){

  // 1. allocate HOST array
  if(argc<1) { printf("usage: cudaShflDown N\n"); exit(0); }
  
  int N = atoi(argv[1]);
  
  int *h_a   = (int*) calloc(N, sizeof(int));
  int *h_res = (int*) calloc(N, sizeof(int));

  for(int n=0;n<N;++n){
    h_a[n] = n;
  }
  
  // 2. allocate DEVICE array
  int *c_a, *c_res;
  cudaMalloc(&c_a,   N*sizeof(int));
  cudaMalloc(&c_res, N*sizeof(int));

  cudaMemcpy(c_a,  h_a,  N*sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(c_res, h_res, N*sizeof(int), cudaMemcpyHostToDevice);
  
  // 3. launch DEVICE collaborate kernel
  int T = 32;
  dim3 G( (N+T-1)/T ); // number of thread blocks to use
  dim3 B( T );

  shflDownWarpKernel <<< G,B >>> (N, c_a, c_res);

  
  // 4. copy data from DEVICE array to HOST array
  cudaMemcpy(h_res, c_res, N*sizeof(int), cudaMemcpyDeviceToHost); 

  // 5. print out values on HOST
  printf("res=[\n");
  for(int n=0;n<N;++n){
    printf("%d ", h_res[n]);
    if( ((n+1)%32==0) ) printf("\n");
  }
  
  // 6. free arrays
  cudaFree(c_a); cudaFree(c_res);
  free(h_a); free(h_res);

  return 0;
}
    
