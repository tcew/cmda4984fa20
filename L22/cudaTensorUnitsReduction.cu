// https://stackoverflow.com/questions/52831987/how-to-use-wmma-functions

#include <cuda.h>
#include <cuda_fp16.h>
#include <mma.h>
#include <iostream>

using namespace nvcuda;

#define DIM 16
#define DIM2 (DIM*DIM)
#define FULL_MASK 0xffffffff
#define SIMD 32

// IN ALL THESE KERNELS ASSUME SIMD THREADS IN THREAD-BLOCK

// thread-block collaboration, one output per thread-block
__global__ void reductionKernelV0(int N, float *a, float *suma){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int G = gridDim.x;

  int M = G*SIMD; // total number of threads
  
  float sum = 0;

  int n = t + b*SIMD;
  
  while(n<N){
    sum += a[n];
    n += M;
  }

  // assume 32 threads per warp
  sum += __shfl_down_sync(FULL_MASK, sum, 16);
  sum += __shfl_down_sync(FULL_MASK, sum,  8);
  sum += __shfl_down_sync(FULL_MASK, sum,  4);
  sum += __shfl_down_sync(FULL_MASK, sum,  2);
  sum += __shfl_down_sync(FULL_MASK, sum,  1);

  if(t==0){
    suma[b] = sum;
  }
}

// block reduction using tensor cores and shfls
__global__ void reductionKernelV1(int N, float *a, float *suma) {

  int e = blockIdx.x;
  int t = threadIdx.x;
  
  // use same A for all thread-blocks, different B and C
  __shared__ half s_R[DIM2], s_Ae[DIM2];
  __shared__ float s_P[DIM2];
  
  // load and convert to half precision
  for(int n=t;n<DIM2;n+=SIMD){
    s_R[n] = __float2half((n<DIM)?1:0);
    int id = n+DIM2*e;
    s_Ae[n] = __float2half((id<N) ? a[id] :0.f);
  }
  
  __syncthreads();
  
  // Declare the fragments
  wmma::fragment<wmma::matrix_a, DIM, DIM, DIM, half, wmma::row_major> R_frag;
  wmma::fragment<wmma::matrix_b, DIM, DIM, DIM, half, wmma::row_major> Ae_frag;
  wmma::fragment<wmma::accumulator, DIM, DIM, DIM, float> P_frag;
  
  // Initialize the output to zero
  wmma::fill_fragment(P_frag, 0.0f);

  // Load the inputs
  wmma::load_matrix_sync(R_frag,  s_R, DIM);
  wmma::load_matrix_sync(Ae_frag, s_Ae, DIM);
  
  // Perform the matrix multiplication (P = R*A)
  wmma::mma_sync(P_frag, R_frag, Ae_frag, P_frag);

  wmma::store_matrix_sync(s_P, P_frag, DIM, wmma::mem_row_major);

  // now have just 16 entries left (could warp shfl those to one value)
  float val = (t<DIM) ? s_P[t] : 0; 
  
  val += __shfl_down_sync(FULL_MASK, val, 8);
  val += __shfl_down_sync(FULL_MASK, val, 4);
  val += __shfl_down_sync(FULL_MASK, val, 2);
  val += __shfl_down_sync(FULL_MASK, val, 1);
  
  if(t==0)
    suma[e] = val;
}


// V2 block reduction using tensor cores twice
__global__ void reductionKernelV2(int N, float *a, float *suma) {

  int e = blockIdx.x;
  int t = threadIdx.x;
  
  // use same A for all thread-blocks, different B and C
  __shared__ half s_R[DIM2], s_Ae[DIM2];
  __shared__ half s_P[DIM2];
  
  // load and convert to half precision
  for(int n=t;n<DIM2;n+=SIMD){
    s_R[n] = (n<DIM)?1:0;
  }

  for(int n=t;n<DIM2;n+=SIMD){
    int id = n+DIM2*e;
    float an = (id<N) ? a[id]:0.f;
    s_Ae[n] = __float2half(an);
  }
  
  __syncthreads();
  
  // Declare the fragments (note using half for all ops)
  wmma::fragment<wmma::matrix_a, DIM, DIM, DIM, half, wmma::row_major> A_frag;
  wmma::fragment<wmma::matrix_b, DIM, DIM, DIM, half, wmma::row_major> B_frag;
  wmma::fragment<wmma::accumulator, DIM, DIM, DIM, half> C_frag;
  
  // Initialize the output to zero
  wmma::fill_fragment(C_frag, 0.f);

  // Load the inputs
  wmma::load_matrix_sync(A_frag,  s_R, DIM);
  wmma::load_matrix_sync(B_frag, s_Ae, DIM);
  
  // Perform the first matrix multiplication (P = R*A)
  wmma::mma_sync(C_frag, A_frag, B_frag, C_frag);

  // store transposed result in P (note transpose on store)
  wmma::store_matrix_sync(s_P, C_frag, DIM, wmma::mem_col_major);

  // load B^t
  wmma::load_matrix_sync(B_frag, s_P, DIM);
  
  // Initialize the output to zero (P_frag will stand in for S)
  wmma::fill_fragment(C_frag, 0.0f);

  // Perform the first matrix multiplication (S = R*P^t)
  wmma::mma_sync(C_frag, A_frag, B_frag, C_frag);

  // store result in P
  wmma::store_matrix_sync(s_P, C_frag, DIM, wmma::mem_col_major);

  if(t==0)
    suma[e] = __half2float(s_P[0]);
}

float reduction(int N, float *c_a, float *c_suma, float *h_suma, int knl){

  int B = 0;
  
  switch(knl){
  case 0:{
    int Nloads = 16;
    B=(N+Nloads*SIMD-1)/(Nloads*SIMD);
    reductionKernelV0<<<B,SIMD>>>(N, c_a, c_suma);
    break;
  }
  case 1:{
    B= (N+DIM2-1)/DIM2;
    reductionKernelV1<<<B,SIMD>>>(N, c_a, c_suma);
    break;
  }
  case 2:{
    B= (N+DIM2-1)/DIM2;
    reductionKernelV2<<<B,SIMD>>>(N, c_a, c_suma);
    break;
  }
  default:{
    printf("Not a valid kernel\n"); exit(-1);
  }
  }
  
  cudaMemcpy(h_suma, c_suma, B*sizeof(float), cudaMemcpyDeviceToHost);
  
  float res = 0;
  for(int e=0;e<B;++e){
    res += h_suma[e];
  }

  return res;
}



int main(int argc, char **argv){

  if(argc!=2){ printf("usage: ./cudaTensorUnitsReduction N\n"); exit(-1);}
  
  int N = atoi(argv[1]);
  int B = (N+DIM2-1)/DIM2;
  
  float *c_a, *h_a, *c_suma, *h_suma;

  h_a = new float[N];
  h_suma = new float[B];

  for (int i = 0; i < N; i++){
    h_a[i] = 2*drand48()-1; // 1.0;
  }

  cudaMalloc(&c_a,    N*sizeof(float));
  cudaMalloc(&c_suma, B*sizeof(float));
  
  cudaMemcpy(c_a, h_a, N*sizeof(float), cudaMemcpyHostToDevice);

  int knl;
  float sumV0 = reduction(N, c_a, c_suma, h_suma, knl=0);
  float sumV1 = reduction(N, c_a, c_suma, h_suma, knl=1);
  float sumV2 = reduction(N, c_a, c_suma, h_suma, knl=2);

  printf("sumV0=%f\n", sumV0);
  printf("sumV1=%f\n", sumV1);
  printf("sumV2=%f\n", sumV2);

}
