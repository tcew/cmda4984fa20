// https://stackoverflow.com/questions/52831987/how-to-use-wmma-functions

#include <cuda.h>
#include <cuda_fp16.h>
#include <mma.h>
#include <iostream>

using namespace nvcuda;

#define DIM 16
#define DIM2 (DIM*DIM)
#define FULL_MASK 0xffffffff
#define SIMD 32

// IN ALL THESE KERNELS ASSUME SIMD THREADS IN THREAD-BLOCK

// thread-block collaboration, one output per thread-block
__global__ void scanKernelV0(int N, float *a, float *scana){

  int t = threadIdx.x;
  int b = blockIdx.x;

  int n = t + b*SIMD;
  
  float sum = (n<N) ? a[n]:0;
  float res;
  
  // assume 32 threads per warp
  res = __shfl_up_sync(FULL_MASK, sum,  1);
  sum += (t>=1)  ? res:0;

  res = __shfl_up_sync(FULL_MASK, sum,  2);
  sum += (t>=2)  ? res:0;
  
  res = __shfl_up_sync(FULL_MASK, sum,  4);
  sum += (t>=4)  ? res:0;
  
  res = __shfl_up_sync(FULL_MASK, sum,  8);
  sum += (t>=8)  ? res:0;
  
  res = __shfl_up_sync(FULL_MASK, sum, 16);
  sum += (t>=16) ? res:0;

  if(n<N)
    scana[n] = sum;

}

// block scan using tensor cores and shfls
__global__ void scanKernelV1(int N, float *a, float *scana) {

  int e = blockIdx.x;
  int t = threadIdx.x;
  

}


void scan(int N, float *c_a, float *c_scana, float *h_scana, int knl){

  int B = 0;
  
  switch(knl){
  case 0:{
    B=(N+SIMD-1)/SIMD;
    scanKernelV0<<<B,SIMD>>>(N, c_a, c_scana);
    break;
  }
  case 1:{
    B= (N+DIM2-1)/DIM2;
    scanKernelV1<<<B,SIMD>>>(N, c_a, c_scana);
    break;
  }
  default:{
    printf("Not a valid kernel\n"); exit(-1);
  }
  }

  cudaMemcpy(h_scana, c_scana, N*sizeof(float), cudaMemcpyDeviceToHost);
  printf("knl %d: ", knl);
  for(int n=0;n<N;++n)
    printf("%f ", h_scana[n]);
  printf("\n");

}



int main(int argc, char **argv){

  if(argc!=2){ printf("usage: ./cudaTensorUnitsScan N\n"); exit(-1);}
  
  int N = atoi(argv[1]);
  
  float *c_a, *h_a, *c_scana, *h_scana;

  h_a     = new float[N];
  h_scana = new float[N];

  for (int i = 0; i < N; i++){
    h_a[i] = 1.;
  }

  cudaMalloc(&c_a,     N*sizeof(float));
  cudaMalloc(&c_scana, N*sizeof(float));
  
  cudaMemcpy(c_a, h_a, N*sizeof(float), cudaMemcpyHostToDevice);

  int knl;
  scan(N, c_a, c_scana, h_scana, knl=0);
  scan(N, c_a, c_scana, h_scana, knl=1);

}
