
#include <stdio.h>
#include <stdlib.h>

__global__ void helloWorldKernel(){

  printf("Hello world!\n");
  
}

int main(int argc, char **argv){
  
  // launch a kernel using one thread-block and one thread
  helloWorldKernel <<< 1, 1 >>> ();

  // make sure print statements flush to stdout
  cudaDeviceSynchronize();
  
  return 1;
}
