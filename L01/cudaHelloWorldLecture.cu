#include <stdio.h>
#include "cuda.h"

// hello world, CUDA version

// GPU kernel function (runs on DEVICE)
__global__ void helloWorldKernel(){

  printf("Hello world from GPU\n");

}

// HOST main function
int main(int argc, char **argv){

  // code in here runs on the CPU (HOST)
  printf("Hello world from CPU\n");

  // HOST queues up kernel for execution on DEVICE
  helloWorldKernel <<< 1 , 1 >>> ();

  // HOST does not know the state of the kernel execution
  // force a synchronization between DEVICE and HOST
  cudaDeviceSynchronize();

  // at this point we know the kernel has finished
  return 0;
}
