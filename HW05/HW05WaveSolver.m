function HW05WaveSolver()

N = 1000;
xmin = -1;
xmax = +1;
finalTime = 2.5;

dx = (xmax-xmin)/(N+1);

x = zeros(N+2,1);
for n=1:N+2
    x(n) = xmin + dx*(n-1);
end


dt = 0.25*dx;

Nsteps = ceil(finalTime/dt);
Nsteps = 2*ceil(Nsteps/2)

dt = finalTime/Nsteps
lambda = (dt/dx)^2;

u = exp(-10*x.^2);
v = 0.5*(exp(-10*(x-dt).^2) + exp(-10*(x+dt).^2));


clf
hold on
for m=0:2:Nsteps-1
    
    v = update(lambda, u, v);
    u = update(lambda, v, u);
    
    if(mod(m,200)==0)
        clf
        plot(x,u);

        axis([-1 1 -1 1])
        drawnow
        
        pause(.01)
    end
end
checksum = dx*norm(u)

return
end

function v = update(lambda, u,uold)

uL = u(1:end-2);
uC = u(2:end-1);
uR = u(3:end);

uoldC = uold(2:end-1);

v = uold;
v(2:end-1) = 2*uC - uoldC + lambda*(uR-2*uC+uL);

return
end