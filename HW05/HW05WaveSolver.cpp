#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// to compile in  serial: g++ -O3 -o HW05WaveSolver HW05WaveSolver.cpp
// to run reference test: ./HW05WaveSolver  50000 -1.0 1.0 2.1

// use second order finite difference in time and space to update solution at entry n
void update(int N, float lambda, float *h_u, float *h_v){

  for(int n=1;n<=N;++n){
    float unold = h_v[n];
    float un = h_u[n];
    h_v[n] = 2.*un - unold + lambda*(h_u[n+1]-2.*un+h_u[n-1]);
  }
  
}

// initialize finite difference nodes
void setup(int N, float xmin, float xmax, float finalTime, float **x, float **u, float **v,
	   float *dt, float *dx){

  // initialize storage (adding boundary nodes at either end)
  float *h_x = (float*) calloc(N+2, sizeof(float));
  float *h_u = (float*) calloc(N+2, sizeof(float));
  float *h_v = (float*) calloc(N+2, sizeof(float));

  *dx = (xmax-xmin)/(N+1);
  printf("dx=%17.15le\n", *dx);

  // choose stable time step
  *dt = 0.25*(*dx);
  
  // adjust dt to take an even integer number of steps
  int Nsteps = ceil(finalTime/(*dt));
  Nsteps = 2*ceil(Nsteps/2.);
  *dt = finalTime/Nsteps;
  
  // initialize coordinates, boundary conditions, and initial conditions
  for(int n=0;n<N+2;++n){
    // x-coordinate of node n
    h_x[n] = xmin + (*dx)*n;
    // solution at time 0 at node n
    h_u[n] = exp(-10.*pow(h_x[n],2));
    // solution at time -dt at node n
    h_v[n] = 0.5*(exp(-10.*pow(h_x[n]-(*dt),2)) + exp(-10.*pow(h_x[n]+(*dt),2))); 
  }
  
  // pass pointers to calling function
  *x = h_x;
  *u = h_u;
  *v = h_v;

}

// time step
void run(int N, float finalTime, float dt, float dx, float *h_u, float *h_v){

  int Nsteps = finalTime/dt;

  printf("Nsteps=%d\n", Nsteps);
  printf("dt=%17.15le\n", dt);
  
  float lambda = pow(dt/dx,2);

  // update recurrence formula twice
  for(int m=0;m<Nsteps;m+=2){

    // first update from u to v 
    update(N, lambda, h_u, h_v);

    // second update from v to u
    update(N, lambda, h_v, h_u);
  }
}

void save(const char *fname, int N, float *h_x, float *h_u){

  FILE *fp = fopen(fname, "w");

  for(int n=0;n<N+2;++n){
    fprintf(fp, "%f %f\n", h_x[n], h_u[n]);
  }
  
  fclose(fp);
}

float checksum(int N, float *h_u){

  float sum = 0;
  for(int n=0;n<N+2;++n){
    sum += h_u[n]*h_u[n];
  }
  return sqrt(sum);
}

// Purpose: solve the linear second order wave equation using a second order finite difference
// in one spatial dimension using a domain [xmin,xmax] with Dirichlet boundary conditions at
// the boundary
int main(int argc, char **argv){

  // read arguments from command line
  if(argc<5) { printf("usage: ./HW05WaveSolver N xmin xmax finalTime\n"); exit(-1); }
  int   N = atoi(argv[1]); // number of points (N+2) total
  float xmin = atof(argv[2]); // x coordinate of left boundary
  float xmax = atof(argv[3]); // x coordinate of right boundary
  float finalTime = atof(argv[4]); 

  // set up finite difference nodes and initial solution
  float *h_x, *h_u, *h_v;
  float dx, dt;
  setup(N, xmin, xmax, finalTime, &h_x, &h_u, &h_v, &dt, &dx);

  // run solver
  run(N, finalTime, dt, dx, h_u, h_v);

  // print solution at final time
  save("solution.dat", N, h_x, h_u);

  // compute checksum
  float sum = dx*checksum(N, h_u);
  printf("checksum=%17.15le\n", sum);
  
  // clean up
  free(h_x); free(h_u); free(h_v);
  
  return 0;
}

  
