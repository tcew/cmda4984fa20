
#include <stdio.h>
#include <stdlib.h>
#include "cuda.h"

// to compile on pascal node with debugging:
// nvcc -g -G -arch=sm_61 -o cudaThreadCommsViaDeviceMemory cudaThreadCommsViaDeviceMemory.cu

// thread ops:
// 1. read from c_a[t + B*b]
// 2. write block reversed c_scratch[B-1-t + B*b]
// 3. read from scratch

__global__ void collaborateKernel(int N, int *c_a, int *c_scratch, int *c_b){

  // find index of thread relative to thread-block
  int t = threadIdx.x;

  // find index of thread-block
  int b = blockIdx.x;

  // find number of threads in thread-block
  int B = blockDim.x;

  // find number of thread-blocks in grid
  int G = gridDim.x;
  
  // construct map from thread and thread-block indices into linear array index
  int n = t + b*B;
  
  int an = c_a[n];

  // write from scratch array in reverse thread order
  int m = B-1-t + B*b;
  c_scratch[m] = an;

  // insert a local barrier to guarantee that the data has been written
  // synchronizes all threads in the single thread-block
  __syncthreads();
  
  // read partially reversed data and write in reverse block order
  int id = t + B*(G-1-b);
  c_b[id] = c_scratch[n]; 
}


int main(int argc, char **argv){

  // 1. allocate HOST array
  int N = 256;
  int *h_a = (int*) calloc(N, sizeof(int));
  int *h_b = (int*) calloc(N, sizeof(int));
  int *h_scratch = (int*) calloc(N, sizeof(int));

  for(int n=0;n<N;++n){
    h_a[n] = n;
    h_b[n] = 0;
    h_scratch[n] = -999;    
  }
  
  // 2. allocate DEVICE array
  int *c_a, *c_b, *c_scratch;
  cudaMalloc(&c_a, N*sizeof(int));
  cudaMalloc(&c_scratch, N*sizeof(int));
  cudaMalloc(&c_b, N*sizeof(int));

  cudaMemcpy(c_a, h_a, N*sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(c_b, h_b, N*sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(c_scratch, h_scratch, N*sizeof(int), cudaMemcpyHostToDevice);
  
  // 3. launch DEVICE collaborate kernel
  int T = 64; // number of threads per thread block 

  dim3 G( (N+T-1)/T ); // number of thread blocks to use
  dim3 B(T);
  
  collaborateKernel <<< G,B >>> (N,  c_a, c_scratch, c_b);

  // 4. copy data from DEVICE array to HOST array
  cudaMemcpy(h_b, c_b, N*sizeof(int), cudaMemcpyDeviceToHost); 

  // 5. print out values on HOST
  for(int n=0;n<N;++n) printf("h_b[%d] = %d\n", n, h_b[n]);

  // 6. free arrays
  cudaFree(c_a); cudaFree(c_b); cudaFree(c_scratch);
  free(h_a); free(h_b);

  return 0;
}
    
