#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include "wave.h"

/*
 make
*/
int main(int argc, char **argv){

  // initialize MPI
  MPI_Init(&argc, &argv);

  // read arguments from command line
  if(argc<8) { printf("usage: ./mpiMain N xmin xmax finalTime kernel sizex sizey\n"); exit(-1); }

  int    N    = atoi(argv[1]); // number of points (N+2) total
  dfloat xmin = atof(argv[2]); // x coordinate of left boundary
  dfloat xmax = atof(argv[3]); // x coordinate of right boundary
  dfloat finalTime = atof(argv[4]);  // final time
  int    knl  = atoi(argv[5]); // kernel number
  int sizex   = atoi(argv[6]);
  int sizey   = atoi(argv[7]);
  dfloat ymin = xmin; // duplicate x range to y range
  dfloat ymax = xmax;

  // setup problem
  wave_t *wave = setup(N, sizex, sizey, xmin, xmax, ymin, ymax, finalTime);

  // run problem
  run(wave, finalTime);
  
  MPI_Finalize();
  exit(0);
}

// initialize finite difference nodes
wave_t *setup(int N,
		      int sizex, int sizey,
		      dfloat xmin, dfloat xmax,
		      dfloat ymin, dfloat ymax,
		      dfloat finalTime){

  wave_t *wave = (wave_t*) calloc(1, sizeof(wave_t));

  int Nall = (N+2)*(N+2);
  wave->N = N;
  wave->Nall = Nall;
  
  // find 2d rank
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  if(size!=sizex*sizey) { printf("Requested %d x %d MPI ranks but %d total MPI ranks, exiting\n", sizex, sizey, size); exit(-1);}
  
  int rankx = rank%sizex;
  int ranky = rank/sizex;
  wave->rankx = rankx;
  wave->ranky = ranky;
  wave->rank = rank;
  
  // determine how many neighbors we have ??
  int Nmessages = 0;
  Nmessages += rankx>0;         // west
  Nmessages += rankx<(sizex-1); // east
  Nmessages += ranky>0;         // south
  Nmessages += ranky<(sizey-1); // north

  int Nmsg = Nmessages*N;
  
  wave->h_indexOut = (int*) calloc(Nmsg, sizeof(int));
  wave->h_indexIn  = (int*) calloc(Nmsg, sizeof(int));
  
  // HOST outgoing halo buffer
  wave->h_dataOut = (dfloat*) malloc(Nmsg*sizeof(dfloat));
  wave->h_dataIn  = (dfloat*) malloc(Nmsg*sizeof(dfloat));

  // message destinations (max 2 destinations)
  wave->h_rankMsg = (int*) calloc(Nmessages, sizeof(int));
  wave->h_Nmsg    = (int*) calloc(Nmessages, sizeof(int));

  int msg = 0, cnt = 0;
  
  if(rankx>0){ // west boundary

    // send/recv message to west
    wave->h_rankMsg[msg] = rank-1;
    wave->h_Nmsg[msg] = N; 

    for(int n=1;n<=N;++n){
      wave->h_indexOut[cnt] = 1 + n*(N+2);
      wave->h_indexIn[cnt] = 0 + n*(N+2);
      ++cnt;
    }

    ++msg;
  }


  if(rankx<sizex-1){ // east  boundary
    
    // send/recv message to east
    wave->h_rankMsg[msg] = rank+1;
    wave->h_Nmsg[msg] = N;

    for(int n=1;n<=N;++n){
      wave->h_indexOut[cnt] = N + n*(N+2);
      wave->h_indexIn[cnt]  = N+1 + n*(N+2);
      ++cnt;
    }

    ++msg;
  }

  
  if(ranky>0){ // south boundary
    
    // send/recv message to south
    wave->h_rankMsg[msg] = rank-sizex;
    wave->h_Nmsg[msg] = N; 

    for(int n=1;n<=N;++n){
      wave->h_indexOut[cnt] = n + (N+2);
      wave->h_indexIn[cnt] = n;
      ++cnt;
    }

    ++msg;
  }


  if(ranky<sizey-1){ // north boundary

    // send/recv message to north
    wave->h_rankMsg[msg] = rank+sizex;
    wave->h_Nmsg[msg] = N; 

    for(int n=1;n<=N;++n){
      wave->h_indexOut[cnt] = n + N*(N+2);
      wave->h_indexIn[cnt] = n + (N+1)*(N+2);
      ++cnt;
    }

    ++msg;
  }

  wave->Nmessages = msg;

  wave->Nmsg = cnt; 

  // initialize storage (adding boundary nodes at either end)
  dfloat *x = (dfloat*) calloc(Nall, sizeof(dfloat));
  dfloat *y = (dfloat*) calloc(Nall, sizeof(dfloat));
  dfloat *u = (dfloat*) calloc(Nall, sizeof(dfloat));
  dfloat *v = (dfloat*) calloc(Nall, sizeof(dfloat));
  dfloat *sumu = (dfloat*) calloc(NBLOCKS, sizeof(dfloat));

  
  wave->h = (xmax-xmin)/(sizex*N+1);
  printf("h=%17.15le\n", wave->h);

  // choose stable time step
  wave->dt = 0.25*(wave->h);
  
  // adjust dt to take an even integer number of steps
  int Nsteps = ceil(finalTime/(wave->dt));
  Nsteps = 2*ceil(Nsteps/2.);
  wave->dt = finalTime/Nsteps;
  
  // initialize coordinates, boundary conditions, and initial conditions
  dfloat xoff = xmin + rankx*N*wave->h;
  dfloat yoff = ymin + ranky*N*wave->h;

  printf("rankx=%d, ranky=%d, xoff=%f, yoff=%f\n", rankx, ranky, xoff, yoff);
  
  for(int j=0;j<N+2;++j){
    for(int i=0;i<N+2;++i){
      // linear index of node (i,j)
      int ij = i + j*(N+2);
      // x-coordinate of node 
      x[ij] = xoff + (wave->h)*i;
      // y-coordinate of node 
      y[ij] = yoff + (wave->h)*j;
      // solution at time 0 at node ij
      u[ij] = 0;
      // compute distance of node from center of pulse
      dfloat xo = .5;
      dfloat yo = .5;
      dfloat rij = sqrt(pow(x[ij]-xo,2)+pow(y[ij]-yo,2));
      dfloat invrij = (rij) ? 1./rij:0;
      
      // solution at time -dt at node n
      dfloat alpha = 140;
      u[ij] = 0;
      v[ij] = 0.2*(exp(-alpha*pow(rij+(wave->dt),2)) -
		     exp(-alpha*pow(rij-(wave->dt),2)))*invrij; 
    }
  }

  // pass pointers to calling function
  wave->h_x = x;
  wave->h_y = y;
  wave->h_u = u;
  wave->h_v = v;
  wave->h_sumu = sumu;

  // initial HIP stuff
  waveInit(wave);
  
  return wave;
}


void exchange(wave_t *wave, dfloat *c_u){

  // extract data for outgoing messages (extract kernel and copy H2D)
  waveExtract(wave, c_u);

  int tag = 999;

  const int maxNmessages = 4;
  MPI_Status  statusesOut[maxNmessages];
  MPI_Status  statusesIn[maxNmessages];
  MPI_Request requestsOut[maxNmessages];
  MPI_Request requestsIn[maxNmessages];

  dfloat *bufOut = wave->h_dataOut;
  for(int msgOut=0;msgOut<wave->Nmessages;++msgOut){
    // non-blocking point to point 
    int dest = wave->h_rankMsg[msgOut];
    int bufN = wave->h_Nmsg[msgOut];

    MPI_Isend(bufOut, bufN, MPI_DFLOAT, dest, tag, MPI_COMM_WORLD, requestsOut+msgOut);

    bufOut += bufN;
  }

  dfloat *bufIn = wave->h_dataIn;
  for(int msgIn=0;msgIn<wave->Nmessages;++msgIn){
    // non-blocking point to point 
    int source = wave->h_rankMsg[msgIn];
    int bufN = wave->h_Nmsg[msgIn];

    MPI_Irecv(bufIn, bufN, MPI_DFLOAT, source, tag, MPI_COMM_WORLD, requestsIn+msgIn);

    bufIn += bufN;
  }

  MPI_Waitall(wave->Nmessages, requestsOut, statusesOut);
  MPI_Waitall(wave->Nmessages,  requestsIn,  statusesIn);

  // inject halo data into host
  waveInject(wave, c_u);
  
}


dfloat computel2(wave_t *wave, dfloat *c_u){
  
  dfloat locall2 = runChecksumKernel(wave->Nall, wave->N, c_u, wave->h_u, wave->c_sumu, wave->h_sumu);

  dfloat globall2;
  
  MPI_Allreduce(&locall2, &globall2, 1, MPI_DFLOAT, MPI_SUM, MPI_COMM_WORLD);
  
  globall2 = wave->h*sqrt(globall2);

  return globall2;		    
}
  
// time step
void run(wave_t *wave, dfloat finalTime){

  int N = wave->N;
  int Nsteps = finalTime/wave->dt;

  printf("Nsteps=%d\n", Nsteps);
  printf("dt=%17.15le\n", wave->dt);
  
  dfloat lambda = pow(wave->dt/wave->h,2);

  hipHostToDevice(wave, wave->h_u, wave->c_u);
  hipHostToDevice(wave, wave->h_v, wave->c_v);

  dfloat l2norm = computel2(wave, wave->c_v);
  
  // update recurrence formula twice
  for(int m=0;m<Nsteps;m+=2){
    
    // first update from u to v
    exchange(wave, wave->c_u);
    runUpdateKernel (N, lambda, wave->c_u, wave->c_v, wave->knl);

    // second update from v to u
    exchange(wave, wave->c_v);
    runUpdateKernel (N, lambda, wave->c_v, wave->c_u, wave->knl);

    
    if((m%50)==0){ // compute l2 norm of solution 

      dfloat l2norm = computel2(wave, wave->c_u);

      int root = 0;
      if(wave->rank==root){
	printf("step: %d, solution norm: %17.15le\n", m+2, l2norm);
      }
    }
    
  }
  
  hipDeviceToHost(wave, wave->c_u, wave->h_u);
  hipDeviceToHost(wave, wave->c_v, wave->h_v);

  save(wave, wave->h_u);
  
}

void save(wave_t *wave, dfloat *h_u){
  char fileName[BUFSIZ];
  sprintf(fileName, "output%05d.dat", wave->rank);
  FILE *fp = fopen(fileName, "w");

  for(int j=0;j<wave->N+2;++j){
    for(int i=0;i<wave->N+2;++i){
      int ij = i + j*(wave->N+2);
      fprintf(fp, "%f %f %f\n", wave->h_x[ij], wave->h_y[ij], h_u[ij]);
    }
  }
  
  fclose(fp);
}

