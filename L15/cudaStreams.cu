


#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

__global__ void simpleKernel(int N, float *c_a, float *c_b){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<N){
    c_b[n] = c_a[n];
  }

}

int main(int argc, char **argv){


  int N = atoi(argv[1]);

  float *h_a, *h_b;
  float *c_a, *c_b, *c_c;

  h_a = (float*) calloc(N, sizeof(float));
  cudaMallocHost(&h_b, N*sizeof(float));

  cudaMalloc(&c_a, N*sizeof(float));
  cudaMalloc(&c_b, N*sizeof(float));
  cudaMalloc(&c_c, N*sizeof(float));

  cudaMemcpy(c_a, h_a, N*sizeof(float), cudaMemcpyHostToDevice);
  
  cudaStream_t stream1, stream2;

  cudaStreamCreate(&stream1);
  cudaStreamCreate(&stream2);

  for(int test=0;test<10;++test){
    // launch kernel on stream 1	
    int T = 256;	   
    dim3 G((N+T-1)/T);
    dim3 B(T);
    simpleKernel <<< G, B, 0, stream1 >>> (N, c_a, c_b);
    
    // overlap simpleKernel with D2H copy
    cudaMemcpyAsync(h_b, c_c, N*sizeof(float), cudaMemcpyDeviceToHost, stream2);
  }
  
  // block until stream1 completes
  cudaStreamSynchronize(stream1);
  
  // block until stream2completes
  cudaStreamSynchronize(stream2);
  
  cudaFreeHost(h_b);
  cudaFree(c_a);
  cudaFree(c_b);
  cudaFree(c_c);

  free(h_a);
}
