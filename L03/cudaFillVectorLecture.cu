
#include <stdio.h>
#include <stdlib.h>
#include "cuda.h"

// fill an array with the same scalar value in each entry
__global__ void fillKernel(int N, double val, double *c_a){

#if 0
  // stoopid serial version (single thread, single thread-block)
  for(int n=0;n<N;++n){
    c_a[n] = val;
  }
#endif

  int t = threadIdx.x;
  int b = blockIdx.x;
  int d = blockDim.x;

  // logic:
  // d is the block size
  // t is the offset in the start of block b

  int n = d*b + t;

  if(n<N)
    c_a[n] = val;
  
}

int main(int argc, char **argv){

  if(argc!=2){ printf("usage: ./cudaFillVectorLecture N\n"); exit(-1);}
  
  int N = atoi(argv[1]);

  double val = 1.2;

  double *h_a = (double*) malloc(N*sizeof(double));

  double *c_a;
  // malloc N*8 bytes in the GPU memory
  cudaMalloc(&c_a, N*sizeof(double)); // cudaMalloc will modify the pointer

  // use a CUDA kernel to fill the entries of the c_a array
  int B = 128; // number of thread per thread-block
  int G = (N+B-1)/B; // number of thread-blocks
  fillKernel <<< G, B >>> (N, val, c_a);

  // block until the kernel is done then copy the answer back to the HOST
  cudaMemcpy(h_a, c_a, N*sizeof(double), cudaMemcpyDeviceToHost);

  printf("G=%d, B=%d\n", G, B);
  for(int n=0;n<N;++n){
    printf("h_a[%d]=%lf\n", n, h_a[n]);
  }

  // free up CUDA arrays
  cudaFree(c_a);

  // free up HOST array
  free(h_a);
  
}
