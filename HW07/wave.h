#define NBLOCKS (28*6)

typedef struct {

  // MPI ranks & sizes
  int rank, rankx, ranky;
  int size, sizex, sizey;
  
  // number of finite difference nodes in each direction excluding halo
  int N;

  // 2d number of FD nodes including halo
  int Nall;

  // kernel index
  int knl;
  
  // mesh size and time step
  dfloat h, dt;
  
  dfloat *h_x;
  dfloat *h_y;
  
  // finite diff data
  dfloat *h_u;
  dfloat *c_u;

  dfloat *h_v;
  dfloat *c_v;

  dfloat *h_sumu;
  dfloat *c_sumu;

  int Nmsg; // amount of data ingoing/outgoing from halo
  
  // list of nodes to extract for outgoing halo
  int *c_indexOut;
  int *h_indexOut;
  
  int *c_indexIn;
  int *h_indexIn;

  // a host halo  array
  dfloat *h_dataOut;
  dfloat *c_dataOut;
  
  dfloat *h_dataIn;
  dfloat *c_dataIn;

  // MPI message info
  int Nmessages;
  int *h_rankMsg;
  int *h_Nmsg;

}wave_t;

wave_t *setup(int N,
	      int sizex, int sizey,
	      dfloat xmin, dfloat xmax,
	      dfloat ymin, dfloat ymax,
	      dfloat finalTime);

void  run(wave_t *exch, dfloat finalTime);
void  save(wave_t *exch, dfloat *h_u);
dfloat computel2(wave_t *exch, dfloat *c_u);
void  exchange(wave_t *exch, dfloat *c_u);

void waveInit(wave_t *exch);

void waveExtract(wave_t *exch, dfloat *c_u);
void waveInject(wave_t *exch, dfloat *c_u);

void cudaHostToDevice(wave_t *exch, dfloat *h_data, dfloat *c_data);
void cudaDeviceToHost(wave_t *exch, dfloat *c_data, dfloat *h_data);

void  runUpdateKernel(int N, dfloat lambda, dfloat *c_u, dfloat *c_v, int knl);
dfloat runChecksumKernel(int N2, int N, dfloat *c_a, dfloat *h_a, dfloat *c_suma, dfloat *h_suma);


