#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

#include "wave.h"

#define SIMD 32
#define FULL_MASK 0xFFFFFFFF
#define NWARPS 32

// initialize information and buffers needed to perform halo exchanges
void waveInit(wave_t *exch){

  // set device ID based on rank
  cudaSetDevice(exch->rank);
  
  // data used for solution on DEVICE
  cudaMalloc(&(exch->c_u), exch->Nall*sizeof(dfloat));
  cudaMalloc(&(exch->c_v), exch->Nall*sizeof(dfloat));

  // allocate CUDA halo info
  cudaMalloc(&(exch->c_indexOut), exch->Nmsg*sizeof(int));
  cudaMalloc(&(exch->c_indexIn), exch->Nmsg*sizeof(int));

  cudaMemcpy(exch->c_indexOut, exch->h_indexOut, exch->Nmsg*sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(exch->c_indexIn,  exch->h_indexIn,  exch->Nmsg*sizeof(int),  cudaMemcpyHostToDevice);

  // DEVICE outgoing halo buffer
  cudaMalloc(&(exch->c_dataOut), exch->Nmsg*sizeof(dfloat));
  cudaMalloc(&(exch->c_dataIn),  exch->Nmsg*sizeof(dfloat));

  // DEVICE allocate reduction array
  cudaMalloc(&(exch->c_sumu), NBLOCKS*sizeof(dfloat));
  
}

__global__ void extractHaloKernel(int Nout, int *c_indexOut,
				  dfloat *c_data, dfloat *c_dataOut){

  
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<Nout){
    int id = c_indexOut[n];
    c_dataOut[n] = c_data[id];
  }
    
}

void waveExtract(wave_t *exch, dfloat *c_data){

  int Nout = exch->Nmsg;
  if(Nout){
    //    printf("Nout=%d\n", Nout);
    int T = 256;
    dim3 G((Nout+T-1)/T);
    dim3 B(T);
    
    extractHaloKernel <<< G, B >>> (Nout,
				    exch->c_indexOut,
				    c_data,
				    exch->c_dataOut);

    cudaMemcpy(exch->h_dataOut, exch->c_dataOut, Nout*sizeof(dfloat),
	       cudaMemcpyDeviceToHost);
  }
}


__global__ void injectHaloKernel(int Nin, int *c_indexIn,
				 dfloat *c_dataIn, dfloat *c_data){

  
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<Nin){
    int id = c_indexIn[n];
    c_data[id] = c_dataIn[n];
  }
    
}

void waveInject(wave_t *exch, dfloat *c_data){

  int Nin = exch->Nmsg;
  if(Nin){
    //    printf("Nin=%d\n", Nin);
    
    int T = 256;
    dim3 G((Nin+T-1)/T);
    dim3 B(T);

    cudaMemcpy(exch->c_dataIn, exch->h_dataIn, Nin*sizeof(dfloat),
	       cudaMemcpyHostToDevice);
    
    injectHaloKernel <<< G, B >>> (Nin,
				   exch->c_indexIn,
				   exch->c_dataIn,
				   c_data);
  }
}

void cudaDeviceToHost(wave_t *exch, dfloat *c_data, dfloat *h_data){

  cudaMemcpy(h_data, c_data, sizeof(dfloat)*exch->Nall, cudaMemcpyDeviceToHost);

}


void cudaHostToDevice(wave_t *exch, dfloat *h_data, dfloat *c_data){

  cudaMemcpy(c_data, h_data, sizeof(dfloat)*exch->Nall, cudaMemcpyHostToDevice);
}


// V0: linear thread indexing
__global__ void updateKernelV0(int N, dfloat lambda, dfloat *c_u, dfloat *c_v){

  // ij = i + j*(N+2)
  int ij = threadIdx.x + blockDim.x*blockIdx.x;

  int ijC = ij;
  int ijN = ij + (N+2);
  int ijS = ij - (N+2);
  int ijE = ij + 1; 
  int ijW = ij - 1;

  // potentially expensive integer operations
  int i = ij % (N+2); // modulo to get i from ij
  int j = ij / (N+2); // integer division to get j from ij

  if(i>0 && i<N+1 && j>0 && j<N+1){

    dfloat uCold = c_v[ijC];
    dfloat uC    = c_u[ijC];
    
    c_v[ijC] = 2.f*uC - uCold
      + lambda*(c_u[ijE]-2.f*uC+c_u[ijW])
      + lambda*(c_u[ijN]-2.f*uC+c_u[ijS]);
  }
}

// V0 launcher
void runUpdateKernelV0(int N, dfloat lambda, dfloat *c_u, dfloat *c_v){

  int Nall = (N+2)*(N+2);
  int T = SIMD*SIMD;
  dim3 G( (Nall+T-1)/T );
  dim3 B(T);

  updateKernelV0 <<<G,B>>> (N, lambda, c_u, c_v);
}


void runUpdateKernel(int N, dfloat lambda, dfloat *c_u, dfloat *c_v, int knl){

  switch(knl){
  case 0: runUpdateKernelV0(N, lambda, c_u, c_v); break;
  default: printf("knl %d not available exiting\n", knl); exit(-1);
  }
  
}





__device__ dfloat blockShflReduction(dfloat sum){
  
  sum += __shfl_down_sync(FULL_MASK, sum, 16);
  sum += __shfl_down_sync(FULL_MASK, sum,  8);
  sum += __shfl_down_sync(FULL_MASK, sum,  4);
  sum += __shfl_down_sync(FULL_MASK, sum,  2);
  sum += __shfl_down_sync(FULL_MASK, sum,  1);

  return sum;
}

// thread-block collaboration, one output per thread-block
__global__ void checksumKernel(int N2, int N, dfloat *c_a, dfloat *c_suma){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;
  int G = gridDim.x;

  int M = G*B; // total number of threads
  
  dfloat sum = 0;

  int n = t + b*B;
  
  while(n<N2){
    int i = n%(N+2);
    int j = n/(N+2);
    if(i>=1 && i<=N && j>=1 && j<=N){
      dfloat an = c_a[n];
      sum += an*an;
    }
    n += M;
  }
  
  // assume 32 threads per warp
  sum = blockShflReduction(sum);
  
  int l = t%32;
  int w = t>>5; // divide by 32

  // now need to share the root values from each warp into the 0 warp
  __shared__ dfloat s_sum[NWARPS];

  if(l==0){
    s_sum[w] = sum;
  }

  // sync here because we intend to read - and the write involved all warps
  __syncthreads(); 

  // nominate one warp to complete the reduction
  if(w==0){ // make warp 0 finalize the block reduction
    sum = s_sum[l];

    sum = blockShflReduction(sum);

    if(t==0){
      c_suma[b] = sum;
    }
  }
}

dfloat  runChecksumKernel(int N2, int N, dfloat *c_a, dfloat *h_a, dfloat *c_suma, dfloat *h_suma){

  int T = NWARPS*SIMD;
  
  dim3 G(NBLOCKS);
  dim3 B(T);      

  checksumKernel <<< G, B >>> ((N+2)*(N+2), N, c_a, c_suma);
  
  cudaMemcpy(h_suma, c_suma, NBLOCKS*sizeof(dfloat), cudaMemcpyDeviceToHost);

  dfloat res = 0;
  for(int n=0;n<NBLOCKS;++n){
    res += h_suma[n];
  }
  return res;
    
}
