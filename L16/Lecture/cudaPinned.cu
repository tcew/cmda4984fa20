
#include <cuda.h>
#include <stdlib.h>
#include <stdio.h>

#define T 256
__global__ void someKernel( int N, float *c_a, float *c_b){

  int t = threadIdx.x;
  int b = blockIdx.x;

  int n = t + b*T;

  if(n<N)
    c_b[n] = c_a[n];
  
}

int main(int argc, char **argv){

  if(argc!=2) {printf("usage: ./cudaPinned N\n"); exit(-1); }
  int N = atoi(argv[1]);
  printf("N=%d\n", N);
  
  float *h_a, *c_a, *c_b, *c_c, *c_d, *c_e;

  // h_a = (float*) malloc(N*sizeof(float));

  cudaStream_t stream1;
  cudaStreamCreate(&stream1);
  
  // allocate page locked memory array on HOST
  cudaMallocHost(&h_a, N*sizeof(float)); 

  // allocate array on DEVICE
  cudaMalloc(&c_a, N*sizeof(float));
  cudaMalloc(&c_b, N*sizeof(float));
  cudaMalloc(&c_c, N*sizeof(float));
  cudaMalloc(&c_d, N*sizeof(float));
  cudaMalloc(&c_e, N*sizeof(float));

  // copy from HOST to DEVICE (non-blocking copy)
  cudaMemcpyAsync(c_a, h_a, N*sizeof(float), cudaMemcpyHostToDevice, stream1);

  dim3 G( (N+T-1)/T);
  dim3 B(T);
  
  // launch kernel into default stream
  someKernel <<< G, B >>> (N, c_b, c_c);

  // launch kernel into stream1 (starts after cudaMemcpyAsync finishes)
  someKernel <<< G, B, 0, stream1 >>> (N, c_d, c_e);
  
  // near immediate return from Async copy. At this point c_a[*] intederminate
  cudaDeviceSynchronize();

  //  free(h_a);
  cudaFree(h_a);
  return 0;
  

}
