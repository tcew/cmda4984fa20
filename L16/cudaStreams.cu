


#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

// simple copy kernel
__global__ void simpleKernel(int N, float *c_a, float *c_b){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<N){
    c_b[n] = c_a[n];
  }

}

int main(int argc, char **argv){

  // read N from the command line args
  if(argc!=2) { printf("usage: ./cudaStreams N\n"); exit(-1); }
  int N = atoi(argv[1]);

  float *h_a, *h_b;

  // allocate heap array on HOST
  h_a = (float*) calloc(N, sizeof(float));

  // allocate "pinned" heap array on HOST 
  cudaMallocHost(&h_b, N*sizeof(float));

  // allocate DEVICE arrays
  float *c_a, *c_b, *c_c;
  cudaMalloc(&c_a, N*sizeof(float));
  cudaMalloc(&c_b, N*sizeof(float));
  cudaMalloc(&c_c, N*sizeof(float));

  // copy data from HOST to DEVICE
  cudaMemcpy(c_a, h_a, N*sizeof(float), cudaMemcpyHostToDevice);

  // create two CUDA streams
  cudaStream_t stream1, stream2;
  cudaStreamCreate(&stream1);
  cudaStreamCreate(&stream2);

  for(int test=0;test<10;++test){
    // launch kernel on stream 1	
    int T = 256;	   
    dim3 G((N+T-1)/T);
    dim3 B(T);

    // launch kernel into CUDA stream1
    simpleKernel <<< G, B, 0, stream1 >>> (N, c_a, c_b);

    // launch async D2H memcpy on CUDA stream2
    cudaMemcpyAsync(h_b, c_c, N*sizeof(float), cudaMemcpyDeviceToHost, stream2);
  }
  
  // block until stream1 completes
  cudaStreamSynchronize(stream1);
  
  // block until stream2 completes
  cudaStreamSynchronize(stream2);

  // garbage collection
  cudaFreeHost(h_b);
  cudaFree(c_a);
  cudaFree(c_b);
  cudaFree(c_c);

  free(h_a);
}
