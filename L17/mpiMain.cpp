
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include "cudaExchange.h"

/* 
#to compile:

nvcc -arch=sm_61 -c cudaExchange.cu
mpicxx.mpich -I. -o mpiMain mpiMain.cpp cudaExchange.o -L/usr/local/cuda-11.0/lib64  -lcuda -lcudart -lstdc++

#to run:

mpiexec.mpich -n 2 ./mpiMain 10

*/

int main(int argc, char **argv){

  int rank, size;
  
  MPI_Init(&argc, &argv);

  if(argc!=2){ printf("usage: ./mpiMain N\n"); exit(-1);};
  
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  int N = atoi(argv[1]);

  printf("rank %d of %d got N=%d\n", rank, size, N);

  float *h_a = (float*) calloc(N+2, sizeof(float));
  for(int n=0;n<N+2;++n)
    h_a[n] = rank;
  
  float *c_a, *c_ahalo, *h_ahaloOut, *h_ahaloIn;

  // initialize DEVICE storage
  cudaSetup(N, &c_a, &c_ahalo, &h_ahaloIn, &h_ahaloOut);

  // copy data to DEVICE
  cudaPutData(N, h_a, c_a);
  
  // get data from DEVICE halo neighbors
  cudaHaloGetData(N, c_a, c_ahalo, h_ahaloOut);

  int tag = 999;
  MPI_Status status;
  
  // send halo data down
  if(rank>0)
    MPI_Send(h_ahaloOut+0, 1, MPI_FLOAT, rank-1, tag, MPI_COMM_WORLD);
  if(rank<size-1)
    MPI_Recv(h_ahaloIn+1, 1, MPI_FLOAT, rank+1, tag, MPI_COMM_WORLD, &status);

  // send data up 
  if(rank<size-1)
    MPI_Send(h_ahaloOut+1, 1, MPI_FLOAT, rank+1, tag, MPI_COMM_WORLD);
  if(rank>0)
    MPI_Recv(h_ahaloIn+0, 1, MPI_FLOAT, rank-1, tag, MPI_COMM_WORLD, &status);
  
  // move incoming data to DEVICE
  cudaHaloPutData(N, h_ahaloIn, c_ahalo, c_a);

  // copy data from DEVICE
  // get data from DEVICE halo neighbors
  cudaGetData(N, c_a, h_a);
  
  for(int n=0;n<N+2;++n){
    printf("rank %d got %f\n", rank, h_a[n]);
  }

  MPI_Finalize();
  return 0;
}
