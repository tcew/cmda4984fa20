
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>


void fooSerial(int N, float *h_a, float *h_b){

  for(int n=0;n<N;++n){
    h_b[n] = h_a[n]*h_a[n] + 2.;
  }
}

__global__ void fooKernel(int N, float *c_a, float *c_b){

  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N)
    c_b[n] = c_a[n]*c_a[n] + 2.;
}


void fooTest(int N){

  float *h_a = (float*) calloc(N, sizeof(float));
  float *h_b = (float*) calloc(N, sizeof(float));
  float *h_cudab = (float*) calloc(N, sizeof(float));

  for(int n=0;n<N;++n){
    h_a[n] = drand48();
    h_b[n] = drand48();
  }
  
  float *c_a, *c_b;

  cudaMalloc(&c_a, N*sizeof(float));
  cudaMalloc(&c_b, N*sizeof(float));

  cudaMemcpy(c_a, h_a, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_b, h_b, N*sizeof(float), cudaMemcpyHostToDevice);

  fooSerial(N, h_a, h_b);

  int T = 256;
  dim3 G( (N+T-1)/T);
  dim3 B(T);
  fooKernel <<< G, B >>> (N, c_a, c_b);

  cudaMemcpy(h_cudab, c_b, N*sizeof(float), cudaMemcpyDeviceToHost);

#define mymax(a,b) (((b)<(a)) ? (a):(b)) 
  
  double maxmismatch = 0;
  double tol =  1e-6;
  
  FILE *fp = fopen("mismatch.dat", "w");
  for(int n=0;n<N;++n){
    double mismatch = fabs(h_cudab[n]-h_b[n]);
    maxmismatch = mymax(maxmismatch, mismatch);
    
    fprintf(fp,"%d %e\n", n, mismatch);
  }
  fclose(fp);
  // dat = load('mismatch.dat');
  
  printf("maxmismatch = %e\n", maxmismatch);
}

int main(int argc, char **argv){

  if(argc!=2) { printf("usage: ./cudaDebugTip N\n"); exit(-1); }
  
  int N = atoi(argv[1]);

  fooTest(N);

  return 0;

}
