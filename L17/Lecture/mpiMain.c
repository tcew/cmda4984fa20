
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>


int main(int argc, char **argv){
  
  MPI_Init(&argc, &argv);

  int rank, size;

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  printf("rank=%d, size=%d\n", rank, size);

  int N = 10;
  float *bufOut = (float*) calloc(N, sizeof(float));  
  float *bufIn  = (float*) calloc(N, sizeof(float));

  for(int n=0;n<N;++n){
    bufOut[n] = rank;
  }  

  int tag = 999;
  MPI_Status status;
    
  // blocking point to point communication (prone to deadlock)
  if(rank+1<size){ // valid target
    int dest = rank+1;
    MPI_Send(bufOut, N, MPI_FLOAT, dest, tag, MPI_COMM_WORLD);
  }
  if(rank-1>=0){

    int source = rank-1;
    MPI_Recv(bufIn, N, MPI_FLOAT, source, tag, MPI_COMM_WORLD, &status);
    printf("rank %d got %f\n", rank, bufIn[0]);
  }

  // non-blocking  point to point comms
  MPI_Request requestOut, requestIn;
  if(rank+1<size){ // valid target
    int dest = rank+1;
    MPI_Isend(bufOut, N, MPI_FLOAT, dest, tag, MPI_COMM_WORLD, &requestOut);
  }
  if(rank-1>=0){
    MPI_Status status;
    int source = rank-1;
    MPI_Irecv(bufIn, N, MPI_FLOAT, source, tag, MPI_COMM_WORLD, &requestIn);
    printf("rank %d got %f\n", rank, bufIn[0]);
  }

  printf("schrodinger's messages\n");
  
  if(rank+1<size)
    MPI_Wait(&requestOut, &status);

  if(rank-1>=0)
    MPI_Wait(&requestIn, &status);
  
  
  
  MPI_Finalize();
  exit(0);
}
