#include <stdio.h>
#include <cuda.h>
#include "cudaExchange.h"

void cudaSetup(int N, float **c_a, float **c_ahalo, float **h_ahaloIn, float **h_ahaloOut){

  cudaMalloc(c_a, (N+2)*sizeof(float));
  cudaMalloc(c_ahalo, 2*sizeof(float));

  *h_ahaloIn  = (float*) calloc(2, sizeof(float));
  *h_ahaloOut = (float*) calloc(2, sizeof(float));
}

void cudaPutData(int N, float *h_a, float *c_a){

  cudaMemcpy(c_a, h_a, (N+2)*sizeof(float), cudaMemcpyHostToDevice);
}

void cudaGetData(int N, float *c_a, float *h_a){

  cudaMemcpy(h_a, c_a, (N+2)*sizeof(float), cudaMemcpyDeviceToHost);
}

// assume two threads
__global__ void getHaloKernel(int N, float *c_a, float *c_ahalo){

  int t = threadIdx.x;
  int id = (t==0) ? 1:N;
  if(t<2)
    c_ahalo[t] = c_a[id];
  
}

void cudaHaloGetData(int N, float *c_a, float *c_ahalo, float *h_ahalo){
  
  // 2 threads
  getHaloKernel <<< 1,2 >>> (N, c_a, c_ahalo);

  // copy data from DEVICE to HOST
  cudaMemcpy(h_ahalo, c_ahalo, 2*sizeof(float), cudaMemcpyDeviceToHost);

}


// assume two threads
__global__ void putHaloKernel(int N, float *c_ahalo, float *c_a){

  int t = threadIdx.x;
  int id = (t==0) ? 0:N+1;
  if(t<2){
    printf("writing %f to %d\n", c_ahalo[t], id);
    c_a[id] = c_ahalo[t];
  }
}


void cudaHaloPutData(int N, float *h_ahalo, float *c_ahalo, float *c_a){

  // copy data from HOST to DEVICE
  cudaMemcpy(c_ahalo, h_ahalo, 2*sizeof(float), cudaMemcpyHostToDevice);

  printf("writing data\n");
  
  // 2 threads
  putHaloKernel <<< 1,2 >>> (N, c_ahalo, c_a);

}
