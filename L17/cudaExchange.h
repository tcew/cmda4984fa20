
void cudaSetup(int N, float **c_a, float **c_ahalo, float **h_ahaloIn, float **h_ahaloOut);

void cudaHaloPutData(int N, float *h_ahalo, float *c_a, float *c_ahalo);
void cudaHaloGetData(int N, float *c_a, float *c_ahalo, float *h_ahalo);

void cudaPutData(int N, float *h_a, float *c_a);
void cudaGetData(int N, float *c_a, float *h_a);
