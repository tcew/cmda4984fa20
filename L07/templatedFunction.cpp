#include <stdio.h>
#include <stdlib.h>

// templated function with extra parameter "N"
template < int N > double templatedFunction(double *x){

  double sumx = 0;
  for(int n=0;n<N;++n){
     sumx += x[n];
  }
  return sumx;
}

int main(int argc, char **argv){

  // declare constant parameter: important that this is known at compile time
  const int N=10;

  // allocate an array
  double *x = (double*) calloc(N, sizeof(double));
  for(int n=0;n<N;++n){
    x[n] = n;
  }
 
  // pass ** known ** parameter as template argument
  double sum = templatedFunction <N> (x);

  // print result
  printf("sum=%g\n", sum);
  
  return 0;  
}
