#include <stdio.h>
#include <stdlib.h>

// templated function with extra parameter "N"
template <class T, int N > T templatedFunction(T *x){

  T sumx = 0;
  for(int n=0;n<N;++n){
     sumx += x[n];
  }
  return sumx;
}

template <class T, int N>
void test(int argc, char **argv){
  
  // allocate an array
  T *x = (T*) calloc(N, sizeof(T));
  for(int n=0;n<N;++n){
    x[n] = n;
  }
 
  // pass ** known ** parameter as template argument
  T sum = templatedFunction <T,N> (x);

  // print result
  printf("sum=%g\n", sum);
}


int main(int argc, char **argv){

  // declare constant parameter: important that this is known at compile time
  const int N=10;

  // call test with T type
  test<double, N> (argc, argv);

  return 0;
}
  
