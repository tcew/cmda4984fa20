
#include <stdio.h>
#include <stdlib.h>

// templating parameter must be known at compile time
template <class ftype, int N>
ftype sum(ftype *x){

  ftype xsum = 0;
  for(int n=0;n<N;++n){
    xsum += x[n];
  }
  return xsum;
}

template <class ftype, int M>
double testSum(){

  ftype *x = (ftype*) calloc(M, sizeof(ftype));
  for(int m=0;m<M;++m){
    x[m] = m;
  }
  
  // call templated sum function
  ftype xsum = sum<ftype, M> (x);
  
  free(x);
  
  return xsum;  
}

int main(int argc, char **argv){

  double a = testSum < double,   10 > ();
  double b = testSum < float,   100 > ();
  double c = testSum < int,    1000 > ();
  double d = testSum < char,    100 > ();

  printf("a=%g\n", a);
  printf("b=%g\n", b);
  printf("c=%g\n", c);
  printf("d=%g\n", d);

  return 0;
}
