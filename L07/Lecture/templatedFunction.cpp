
#include <stdio.h>
#include <stdlib.h>

// templating parameter must be known at compile time
template <int N>
double sum(double *x){

  double xsum = 0;
  for(int n=0;n<N;++n){
    xsum += x[n];
  }
  return xsum;
}

int main(int argc, char **argv){

  const int M = 100;

  double *x = (double*) calloc(M, sizeof(double));
  for(int m=0;m<M;++m){
    x[m] = m;
  }

  // call templated sum function
  double xsum = sum<M> (x);

  // print out result
  printf("xsum = %g\n", xsum);

  return 0;  
}
