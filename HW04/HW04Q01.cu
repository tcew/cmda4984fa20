#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <unistd.h>

#define Nloops 200

__global__ void kernel1(int N, float alpha, float *c_x){

  // find index of thread relative to thread-block
  int t = threadIdx.x;
  
  // find index of thread-block
  int b = blockIdx.x;

  // find number of threads in thread-block
  int B = blockDim.x;

  // construct map from thread and thread-block indices into linear array index
  int n = t + b*B;
  
  float  x = alpha*n;
  
#pragma unroll 200
  for(int i=0;i<Nloops;++i){
    x += x*alpha;
  }

  if(x==0.1234567f && n<N){
    c_x[n] = x;
  }
}

void testKernel1(int N, float a, float *c_x){

  int B = 256;
  int G = (N+B-1)/B;

  kernel1 <<< G, B >>> (N, a, c_x);

  usleep(3e5);

  int Ntests = 10;

  for(int test=1;test<=Ntests;++test){
    kernel1 <<< G, B >>> (N, a, c_x);
  }
  
}


__global__ void kernel2(int N, double alpha, double *c_x){

  // find index of thread relative to thread-block
  int t = threadIdx.x;
  
  // find index of thread-block
  int b = blockIdx.x;

  // find number of threads in thread-block
  int B = blockDim.x;

  // construct map from thread and thread-block indices into linear array index
  int n = t + b*B;
  
  double  x = alpha*n;
  
#pragma unroll 200
  for(int i=0;i<Nloops;++i){
    x += x*alpha;
  }

  if(x==0.1234567 && n<N){
    c_x[n] = x;
  }
}

void testKernel2(int N, double a, double *c_x){

  int B = 256;
  int G = (N+B-1)/B;

  kernel2 <<< G, B >>> (N, a, c_x);

  usleep(3e5);

  int Ntests = 10;

  for(int test=1;test<=Ntests;++test){
    kernel2 <<< G, B >>> (N, a, c_x);
  }
  
}



__global__ void kernel3(int N, float alpha, float *c_x){

  // find index of thread relative to thread-block
  int t = threadIdx.x;
  
  // find index of thread-block
  int b = blockIdx.x;

  // find number of threads in thread-block
  int B = blockDim.x;

  // construct map from thread and thread-block indices into linear array index
  int n = t + b*B;
  
  float x = alpha*n;
  
#pragma unroll 200
  for(int i=0;i<Nloops;++i){
    x = cos(alpha*x);
  }

  if(x==0.1234567 && n<N){
    c_x[n] = x;
  }
}

void testKernel3(int N, float a, float *c_x){

  int B = 256;
  int G = (N+B-1)/B;

  kernel3 <<< G, B >>> (N, a, c_x);

  usleep(3e5);

  int Ntests = 10;

  for(int test=1;test<=Ntests;++test){
    kernel3 <<< G, B >>> (N, a, c_x);
  }
  
}





int main(int argc, char **argv){

  int N = 1000000;

  float fa = 1.0;
  float *c_fx;
  float *c_fy;

  double da = 1.0;
  double *c_dx;
  double *c_dy;

  cudaMalloc(&c_fx, N*sizeof(float));
  cudaMalloc(&c_fy, N*sizeof(float));
  cudaMalloc(&c_dx, N*sizeof(double));
  cudaMalloc(&c_dy, N*sizeof(double));
  
  testKernel1(N, fa, c_fx);
  
  testKernel2(N, da, c_dx);

  testKernel3(N, fa, c_fx);

  cudaDeviceSynchronize();
  return 0;
}
