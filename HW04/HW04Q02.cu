#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <unistd.h>

__global__ void kernel1(int N, float alpha, float *c_x){

  // find index of thread relative to thread-block
  int t = threadIdx.x;
  
  // find index of thread-block
  int b = blockIdx.x;

  // find number of threads in thread-block
  int B = blockIdx.x;

  // construct map from thread and thread-block indices into linear array index
  int n = t + b*B;
  
  c_x[n] = alpha;
}

void testKernel1(int N, float a, float *c_x){

  int B = 32;
  int G = (N+B-1)/B;

  kernel1 <<< G, B >>> (N, a, c_x);

  usleep(3e5);

  int Ntests = 10;

  for(int test=1;test<=Ntests;++test){
    kernel1 <<< G, B >>> (N, a, c_x);
  }
  
}


int main(int argc, char **argv){

  int N = 3200;

  float fa = 1.0;
  float *c_fx;
  float *c_fy;

  cudaMalloc(&c_fx, N*sizeof(float));
  cudaMalloc(&c_fy, N*sizeof(float));
  
  testKernel1(N, fa, c_fx);
  
  cudaDeviceSynchronize();
  return 0;
}
