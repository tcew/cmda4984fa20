#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "cuda.h"

// to compile on pascal node:
// nvcc -o cudaFlops cudaFlops.cu -lm

template <int Nloops>
__global__ void flopKernel(int N, float alpha, float *c_x){
  
  // find index of thread relative to thread-block
  int t = threadIdx.x;

  // find index of thread-block
  int b = blockIdx.x;

  // find number of threads in thread-block
  int B = blockDim.x;

  // construct map from thread and thread-block indices into linear array index
  int n = t + b*B;
  
  float x = alpha*n;
  
#pragma unroll Nloops
  for(int i=0;i<Nloops;++i){
    x += x*alpha;
  }

  if(x==0.1234567){
    c_x[n] = x;
  }
}


#define mymin(a,b) ( (a)<(b) ? (a):(b) )

int main(int argc, char **argv){

  // 0. to run with arrays of length 1024:
  // ./cudaVectorOpV1 1024
  if(argc!=2){ printf("usage: ./cudaFlops N\n"); exit(-1);}

  cudaSetDevice(1); // choose your assigned DEVICE
  
  // 1. allocate HOST array
  int N = atoi(argv[1]);
  float *h_x = (float*) calloc(N, sizeof(float));

  // 2. populate HOST arrays
  for(int n=0;n<N;++n){
    h_x[n] = drand48();
  }
  
  // 3. allocate DEVICE array
  float *c_x;
  cudaMalloc(&c_x, N*sizeof(float));

  // 4. copy data from HOST to DEVICE
  cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);
  
  // 5. launch DEVICE fill kernel
  int T = 256; // number of threads per thread block 
  dim3 G( (N+T-1)/T ); // number of thread blocks to use
  dim3 B(T);

  float alpha = 1.2;

  const int Nloops = 200;
  
  // 6. warm up run (copy kernel code to DEVICE)
  int Nwarm = 5;
  for(int n=0;n<Nwarm;++n)
    flopKernel<Nloops> <<< G,B >>> (N, alpha, c_x);
  
  // 7. create CUDA events
  cudaEvent_t tic, toc;
  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  // 8. insert tic event into GPU queue
  cudaEventRecord(tic);

  // 9. run kernel 10 times
  int Nrun = 10;
  for(int n=0;n<Nrun;++n)
    flopKernel<Nloops> <<< G,B >>> (N, alpha, c_x);

  // 10. insert toc event into GPU queue
  cudaEventRecord(toc);

  // 11. make sure HOST and DEVICE are synced 
  cudaDeviceSynchronize();

  // 12. estimate time take
  float time;
  cudaEventElapsedTime(&time, tic, toc);
  time = time/1000; // convert to seconds
  
  // 13. estimate flops
  long long int flops = Nrun*(1+Nloops*2)*(long long int)N; 

  // 14. simple estimate of bandwidth for this problem size
  float GFLOPSs = flops/(1.e9*time);

  // 15. print
  printf("%g, %g, %g %%%% GFLOPS, T (s), GFLOPS/s\n",
	 flops/1.e9, time, GFLOPSs);
  
  // 16. free arrays
  cudaFree(c_x);
  free(h_x); 

  return 0;
}
    
